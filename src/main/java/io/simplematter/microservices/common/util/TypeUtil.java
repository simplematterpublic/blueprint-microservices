package io.simplematter.microservices.common.util;

public class TypeUtil {

    public static int toInteger(final Object value) {

        if (value instanceof Integer) {
            return (Integer) value;
        } else if (value instanceof Long) {
            return ((Long) value).intValue();
        } else {
            throw new RuntimeException("Unknown type " + value.getClass().getName());
        }
    }

    public static long toLong(final Object value) {

        if (value instanceof Long) {
            return (Long) value;
        } else if (value instanceof Integer) {
            return new Long((Integer) value);
        } else {
            throw new RuntimeException("Unknown type " + value.getClass().getName());
        }
    }

    public static double toDouble(final Object value) {

        if (value instanceof Double) {
            return (Double) value;
        } else if (value instanceof Integer) {
            return new Double((Integer) value);
        } else if (value instanceof Long) {
            return new Double((Long) value);
        } else {
            throw new RuntimeException("Unknown type " + value.getClass().getName());
        }
    }
}
