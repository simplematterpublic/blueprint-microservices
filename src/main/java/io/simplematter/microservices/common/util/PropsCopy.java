package io.simplematter.microservices.common.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropsCopy {

    public static Properties copy(final Properties properties) {

        final Properties props = new Properties();
        props.putAll(properties);
        return props;
    }

    public static Properties copy(final Properties properties, final Map<String, String> map) {

        final Properties props = copy(properties);
        map.forEach((k, v) -> {
            props.put(k, v);
        });
        return props;
    }

    public static Properties copy(final Properties properties, String key, String value) {

        final Map<String, String> map = new HashMap<>();
        map.put(key, value);
        return copy(properties, map);
    }


}
