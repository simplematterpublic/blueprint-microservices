package io.simplematter.microservices.common.projection;

import java.util.List;

public interface Store<K, V> {

    void persist(V value);

    V byId(K value);

    List<V> byIds(List<K> values);

    List<V> all(int limit);
}
