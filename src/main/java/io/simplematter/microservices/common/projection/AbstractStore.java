package io.simplematter.microservices.common.projection;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.model.MapObject;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.Cursor;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class AbstractStore<K, V> implements Store<K, V> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final RethinkDB r = RethinkDB.r;
    private final Connection connection;
    private final String table;

    public AbstractStore(final Connection connection, final String table) {

        this.connection = connection;
        this.table = table;
    }

    public String getTable() {

        return table;
    }

    @Override
    public void persist(final V entity) {

        //logger.info("Writing snapshot id: " + entity.getId());
        r.table(getTable()).insert(convert(entity))
                .optArg("conflict", "replace").run(connection);
    }

    /**
     * Return the Payment object given the id
     *
     * @param id The Payment id
     * @return The Payment object if exists
     */
    @Override
    public V byId(final K id) {

        final HashMap entity = r.table(getTable()).get(id).run(connection);
        return entity != null ? convert(entity) : null;
    }

    @Override
    public List<V> byIds(final List<K> ids) {

        final Cursor<HashMap> entities = r.table(getTable())
                .getAll(ids.toArray(new String[ids.size()]))
                .optArg("index", "id").run(connection);
        final List<V> payments = new ArrayList();
        entities.forEach(entity -> {
            payments.add(convert(entity));
        });
        entities.close();
        return payments;
    }

    @Override
    public List<V> all(final int limit) {

        final List<HashMap> entities = r.table(getTable()).
                orderBy(r.desc("timestamp")).limit(limit).run(connection);
        final List<V> payments = new ArrayList();
        entities.forEach(entity -> {
            payments.add(convert(entity));
        });
        return payments;
    }

    protected abstract V convert(final HashMap source);

    protected abstract MapObject convert(final V source);
}
