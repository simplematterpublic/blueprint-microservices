package io.simplematter.microservices.common.kafka.serialization;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.protocol.Converter;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

//import io.confluent.kafka.serializers.KafkaAvroSerializer;

public class AvroSerializer<T> implements Serializer<T> {

    private final String applicationId;
    private final KafkaAvroSerializer serializer;
    private final Converter converter;

    public AvroSerializer(final String applicationId, final SchemaRegistryClient schemaRegistry,
                          final Converter converter) {

        this.applicationId = applicationId;
        this.serializer = new KafkaAvroSerializer(schemaRegistry, "OrderMaterializer");
        this.converter = converter;
    }

    @Override
    public void configure(final Map<String, ?> configs, final boolean isKey) {

        serializer.configure(configs, isKey);
    }

    @Override
    public byte[] serialize(String topic, final T record) {

        if (topic.startsWith("KSTREAM-")) {
            topic = applicationId + "-" + topic;
        }
        //System.out.println("Writing to " + topic);
        return serializer.serialize(topic, converter.serialize(record));
    }

    @Override
    public void close() {
        serializer.close();
    }
}