package io.simplematter.microservices.common.kafka.serialization;

import io.confluent.common.config.ConfigException;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import io.confluent.kafka.serializers.NonRecordContainer;
import kafka.utils.VerifiableProperties;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.common.errors.SerializationException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;

public abstract class AbstractKafkaAvroSerializer extends AbstractKafkaAvroSerDes {

    private final EncoderFactory encoderFactory = EncoderFactory.get();

    public AbstractKafkaAvroSerializer() {

    }

    protected void configure(KafkaAvroSerializerConfig config) {
        this.configureClientProperties(config);
    }

    protected KafkaAvroSerializerConfig serializerConfig(Map<String, ?> props) {
        try {
            return new KafkaAvroSerializerConfig(props);
        } catch (ConfigException var3) {
            throw new org.apache.kafka.common.config.ConfigException(var3.getMessage());
        }
    }

    protected KafkaAvroSerializerConfig serializerConfig(VerifiableProperties props) {
        try {
            return new KafkaAvroSerializerConfig(props.props());
        } catch (ConfigException var3) {
            throw new org.apache.kafka.common.config.ConfigException(var3.getMessage());
        }
    }

    protected byte[] serializeImpl(String subject, Object object) throws SerializationException {
        Schema schema = null;
        if(object == null) {
            return null;
        } else {
            try {
                schema = this.getSchema(object);
                //SchemaMetadata metadata = schemaRegistry.getLatestSchemaMetadata(subject);
                //int e = -1;
                //if (metadata == null) {
                int e = this.schemaRegistry.register(subject, schema);
                //} else {
                //    e = metadata.getId();
                //}
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                out.write(0);
                out.write(ByteBuffer.allocate(4).putInt(e).array());
                if(object instanceof byte[]) {
                    out.write((byte[])((byte[])object));
                } else {
                    BinaryEncoder bytes = this.encoderFactory.directBinaryEncoder(out, (BinaryEncoder)null);
                    Object value = object instanceof NonRecordContainer ?((NonRecordContainer)object).getValue():object;
                    Object writer;
                    if(value instanceof SpecificRecord) {
                        writer = new SpecificDatumWriter(schema);
                    } else {
                        writer = new GenericDatumWriter(schema);
                    }

                    ((DatumWriter)writer).write(value, bytes);
                    bytes.flush();
                }

                byte[] bytes1 = out.toByteArray();
                out.close();
                return bytes1;
            } catch (RuntimeException | IOException var9) {
                throw new SerializationException("Error serializing Avro message", var9);
            } catch (RestClientException var10) {
                throw new SerializationException("Error registering Avro schema: " + schema, var10);
            }
        }
    }
}
