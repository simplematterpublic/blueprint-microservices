package io.simplematter.microservices.common.kafka.serialization;


import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.serializers.*;
import org.apache.avro.Schema;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class KafkaAvroDeserializer extends AbstractKafkaAvroDeserializer implements Deserializer<Object> {

    private String applicationId;
    private boolean isKey;

    public KafkaAvroDeserializer(String applicationId) {
        this.applicationId = applicationId;
    }

    public KafkaAvroDeserializer(SchemaRegistryClient client, String applicationId) {
        this.schemaRegistry = client;
        this.applicationId = applicationId;
    }

    public KafkaAvroDeserializer(SchemaRegistryClient client, Map<String, ?> props) {
        this.schemaRegistry = client;
        this.configure(this.deserializerConfig(props));
    }

    public void configure(Map<String, ?> configs, boolean isKey) {
        this.isKey = isKey;
        this.configure(new KafkaAvroDeserializerConfig(configs));
    }

    public Object deserialize(String s, byte[] bytes) {
        return super.deserialize(applicationId, bytes);
    }

    public Object deserialize(String s, byte[] bytes, Schema readerSchema) {
        return super.deserialize(applicationId, bytes, readerSchema);
    }

    public void close() {
    }
}
