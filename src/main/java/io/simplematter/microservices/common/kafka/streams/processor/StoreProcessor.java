package io.simplematter.microservices.common.kafka.streams.processor;

import io.simplematter.microservices.common.protocol.Envelope;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;

public abstract class StoreProcessor<K, V, S> implements Processor<K, V> {

    private ProcessorContext context;

    @Override
    public void init(ProcessorContext processorContext) {

        this.context = processorContext;
    }

    protected abstract String getStoreName();

    protected final ProcessorContext context() {
        return context;
    }

    protected KeyValueStore<K, S> getStore() {

        return (KeyValueStore<K, S>) context().getStateStore(getStoreName());
    }

    protected <T> T getState(final K key) {

        final S currentState = getStore().get(key);
        return currentState == null ? null : (T) ((Envelope) currentState).getPayload();
    }

    @Override
    public void punctuate(long interval) {

    }

    @Override
    public void close() {

        if (getStore().isOpen()) {
            getStore().close();
        }
    }
}
