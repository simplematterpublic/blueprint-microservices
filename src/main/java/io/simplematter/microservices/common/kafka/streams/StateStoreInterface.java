package io.simplematter.microservices.common.kafka.streams;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;

import java.util.ArrayList;
import java.util.List;

public class StateStoreInterface<K, V> {

    private final KafkaStreams stream;
    private final MetadataService metadataService;

    public StateStoreInterface(final KafkaStreams stream) {

        this.stream = stream;
        this.metadataService = new MetadataService(stream);
    }

    public List<HostStoreInfo> metadata() {

        return metadataService.streamsMetadata();
    }

    public List<HostStoreInfo> stores(final String name) {

        return metadataService.streamsMetadataForStore(name);
    }

    // TODO: Check
    public HostStoreInfo stores(final String name, final K key) {

        return metadataService.streamsMetadataForStoreAndKey(name, key, null);
    }

    public V byKey(final String name, final K key) {

        return (V) stream.store(name, QueryableStoreTypes.keyValueStore()).get(key);
    }

    public List<V> all(final String name) {

        final ReadOnlyKeyValueStore<Object, Object> store = stream.store(name, QueryableStoreTypes.keyValueStore());
        final KeyValueIterator iterator = store.all();
        final List<V> all = new ArrayList();
        while (iterator.hasNext()) {
            all.add((V) iterator.next());
        }
        return all;
    }
}

/*
class StoreInterface[K, V](streams: KafkaStreams) {

  private val metadataService = new MetadataService(streams)

  def metadata: Seq[HostStoreInfo] = {
    metadataService.streamsMetadata
  }

  def stores(name: String): Seq[HostStoreInfo] = {
    metadataService.streamsMetadataForStore(name)
  }

  // TODO: Check
  def stores(name: String, key: K): HostStoreInfo = {
    metadataService.streamsMetadataForStoreAndKey(name, key, null)
  }

  def byKey(name: String, key: K): V = {
    val store = streams.store(name, QueryableStoreTypes.keyValueStore[K, V]())
    store.get(key)
  }

  def range(name: String, from: K, to: K): Seq[V] = {
    val store = streams.store(name, QueryableStoreTypes.keyValueStore[K, V]())
    val results = store.range(from, to)
    results.asScala.toSeq map (_.value)
  }

  def windowed(name: String, key: K, from: Long, to: Long): Seq[V] = {
    val store = streams.store(name, QueryableStoreTypes.windowStore[K, V]())
    val results = store.fetch(key, from, to)
    results.asScala.toSeq map (_.value)
  }

  def all(name: String): Seq[V] = {
    val store = streams.store(name, QueryableStoreTypes.keyValueStore[K, V]())
    val all = store.all()
    all.asScala.toSeq map (_.value)
  }
}

 */
