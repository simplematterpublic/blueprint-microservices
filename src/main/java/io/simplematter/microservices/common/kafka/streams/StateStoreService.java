package io.simplematter.microservices.common.kafka.streams;

import org.apache.kafka.streams.KafkaStreams;

public class StateStoreService<K, V> extends StateStoreInterface<K, V> {

    public StateStoreService(final KafkaStreams stream) {
        super(stream);
    }
}
