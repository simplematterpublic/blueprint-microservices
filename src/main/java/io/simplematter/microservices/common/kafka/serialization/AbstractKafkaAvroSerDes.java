package io.simplematter.microservices.common.kafka.serialization;

import io.confluent.common.config.ConfigException;
import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericContainer;
import org.apache.kafka.common.errors.SerializationException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractKafkaAvroSerDes {

    protected static final byte MAGIC_BYTE = 0;
    protected static final int idSize = 4;
    private static final Map<String, Schema> primitiveSchemas;
    protected SchemaRegistryClient schemaRegistry;

    public AbstractKafkaAvroSerDes() {
    }

    private static Schema createPrimitiveSchema(Schema.Parser parser, String type) {
        String schemaString = String.format("{\"type\" : \"%s\"}", new Object[]{type});
        return parser.parse(schemaString);
    }

    protected void configureClientProperties(AbstractKafkaAvroSerDeConfig config) {
        try {
            List e = config.getSchemaRegistryUrls();
            int maxSchemaObject = config.getMaxSchemasPerSubject();
            if(null == this.schemaRegistry) {
                this.schemaRegistry = new CachedSchemaRegistryClient(e, maxSchemaObject);
            }

        } catch (ConfigException var4) {
            throw new org.apache.kafka.common.config.ConfigException(var4.getMessage());
        }
    }

    protected static String getSubjectName(String applicationId, String topic, boolean isKey) {
        /*
        if (topic.startsWith("KSTREAM-")) {
            topic = applicationId + "-" + topic;
        } */
        return isKey?topic + "-key":topic + "-value";
    }

    protected static String getOldSubjectName(Object value) {
        if(value instanceof GenericContainer) {
            return ((GenericContainer)value).getSchema().getName() + "-value";
        } else {
            throw new SerializationException("Primitive types are not supported yet");
        }
    }

    protected Schema getSchema(Object object) {
        if(object == null) {
            return (Schema)primitiveSchemas.get("Null");
        } else if(object instanceof Boolean) {
            return (Schema)primitiveSchemas.get("Boolean");
        } else if(object instanceof Integer) {
            return (Schema)primitiveSchemas.get("Integer");
        } else if(object instanceof Long) {
            return (Schema)primitiveSchemas.get("Long");
        } else if(object instanceof Float) {
            return (Schema)primitiveSchemas.get("Float");
        } else if(object instanceof Double) {
            return (Schema)primitiveSchemas.get("Double");
        } else if(object instanceof CharSequence) {
            return (Schema)primitiveSchemas.get("String");
        } else if(object instanceof byte[]) {
            return (Schema)primitiveSchemas.get("Bytes");
        } else if(object instanceof GenericContainer) {
            return ((GenericContainer)object).getSchema();
        } else {
            throw new IllegalArgumentException("Unsupported Avro type. Supported types are null, Boolean, Integer, Long, Float, Double, String, byte[] and IndexedRecord");
        }
    }

    public int register(String subject, Schema schema) throws IOException, RestClientException {
        return this.schemaRegistry.register(subject, schema);
    }

    public Schema getByID(int id) throws IOException, RestClientException {
        return this.schemaRegistry.getByID(id);
    }

    static {
        Schema.Parser parser = new Schema.Parser();
        primitiveSchemas = new HashMap();
        primitiveSchemas.put("Null", createPrimitiveSchema(parser, "null"));
        primitiveSchemas.put("Boolean", createPrimitiveSchema(parser, "boolean"));
        primitiveSchemas.put("Integer", createPrimitiveSchema(parser, "int"));
        primitiveSchemas.put("Long", createPrimitiveSchema(parser, "long"));
        primitiveSchemas.put("Float", createPrimitiveSchema(parser, "float"));
        primitiveSchemas.put("Double", createPrimitiveSchema(parser, "double"));
        primitiveSchemas.put("String", createPrimitiveSchema(parser, "string"));
        primitiveSchemas.put("Bytes", createPrimitiveSchema(parser, "bytes"));
    }
}
