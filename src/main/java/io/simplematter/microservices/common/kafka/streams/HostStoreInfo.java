package io.simplematter.microservices.common.kafka.streams;

import java.util.Set;

public class HostStoreInfo {

    private final String host;
    private final int port;
    private final Set<String> names;

    public HostStoreInfo(final String host, final int port, final Set<String> names) {

        this.host = host;
        this.port = port;
        this.names = names;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public Set<String> getNames() {
        return names;
    }
}
