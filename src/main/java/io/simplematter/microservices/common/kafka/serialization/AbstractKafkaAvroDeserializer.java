package io.simplematter.microservices.common.kafka.serialization;

import io.confluent.common.config.ConfigException;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.NonRecordContainer;
import kafka.utils.VerifiableProperties;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericContainer;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.common.errors.SerializationException;
import org.codehaus.jackson.node.JsonNodeFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class AbstractKafkaAvroDeserializer extends AbstractKafkaAvroSerDes {

    public static final String SCHEMA_REGISTRY_SCHEMA_VERSION_PROP = "schema.registry.schema.version";
    private final DecoderFactory decoderFactory = DecoderFactory.get();
    protected boolean useSpecificAvroReader = false;
    private final Map<String, Schema> readerSchemaCache = new ConcurrentHashMap();

    public AbstractKafkaAvroDeserializer() {
    }

    protected void configure(KafkaAvroDeserializerConfig config) {
        this.configureClientProperties(config);
        this.useSpecificAvroReader = config.getBoolean("specific.avro.reader");
    }

    protected KafkaAvroDeserializerConfig deserializerConfig(Map<String, ?> props) {
        try {
            return new KafkaAvroDeserializerConfig(props);
        } catch (ConfigException var3) {
            throw new org.apache.kafka.common.config.ConfigException(var3.getMessage());
        }
    }

    protected KafkaAvroDeserializerConfig deserializerConfig(VerifiableProperties props) {
        try {
            return new KafkaAvroDeserializerConfig(props.props());
        } catch (ConfigException var3) {
            throw new org.apache.kafka.common.config.ConfigException(var3.getMessage());
        }
    }

    private ByteBuffer getByteBuffer(byte[] payload) {
        ByteBuffer buffer = ByteBuffer.wrap(payload);
        if(buffer.get() != 0) {
            throw new SerializationException("Unknown magic byte!");
        } else {
            return buffer;
        }
    }

    protected Object deserialize(String applicationId, byte[] payload) throws SerializationException {
        return this.deserialize(applicationId, false, (String)null, (Boolean)null, payload, (Schema)null);
    }

    protected Object deserialize(String applicationId, byte[] payload, Schema readerSchema) throws SerializationException {
        return this.deserialize(applicationId, false, (String)null, (Boolean)null, payload, readerSchema);
    }

    protected Object deserialize(String applicationId, boolean includeSchemaAndVersion, String topic, Boolean isKey, byte[] payload, Schema readerSchema) throws SerializationException {
        if(payload == null) {
            return null;
        } else {
            byte id = -1;

            try {
                if (topic != null && topic.startsWith("MATERIALIZER")) {
                    System.out.println(topic);
                }
                ByteBuffer e = this.getByteBuffer(payload);
                int id1 = e.getInt();
                Schema schema = this.schemaRegistry.getByID(id1);
                int length = e.limit() - 1 - 4;
                Object result;
                if(schema.getType().equals(Schema.Type.BYTES)) {
                    byte[] version = new byte[length];
                    e.get(version, 0, length);
                    result = version;
                } else {
                    int version1 = e.position() + e.arrayOffset();
                    DatumReader i$ = this.getDatumReader(schema, readerSchema);
                    Object memberSchema = i$.read((Object)null, this.decoderFactory.binaryDecoder(e.array(), version1, length, (BinaryDecoder)null));
                    if(schema.getType().equals(Schema.Type.STRING)) {
                        memberSchema = memberSchema.toString();
                    }

                    result = memberSchema;
                }

                if(!includeSchemaAndVersion) {
                    return result;
                } else {
                    Integer version2 = Integer.valueOf(this.schemaRegistry.getVersion(getSubjectName(applicationId, topic, isKey.booleanValue()), schema));
                    if(schema.getType() == Schema.Type.UNION) {
                        Iterator i$1 = schema.getTypes().iterator();

                        while(i$1.hasNext()) {
                            Schema memberSchema1 = (Schema)i$1.next();
                            if(memberSchema1.getType() != Schema.Type.NULL) {
                                memberSchema1.addProp("schema.registry.schema.version", JsonNodeFactory.instance.numberNode(version2));
                                break;
                            }
                        }
                    } else {
                        schema.addProp("schema.registry.schema.version", JsonNodeFactory.instance.numberNode(version2));
                    }

                    return schema.getType().equals(Schema.Type.RECORD)?result:new NonRecordContainer(schema, result);
                }
            } catch (RuntimeException | IOException var14) {
                throw new SerializationException("Error deserializing Avro message for id " + id, var14);
            } catch (RestClientException var15) {
                throw new SerializationException("Error retrieving Avro schema for id " + id, var15);
            }
        }
    }

    protected GenericContainer deserializeWithSchemaAndVersion(String applicationId, String topic, boolean isKey, byte[] payload) throws SerializationException {
        return (GenericContainer)this.deserialize(applicationId, true, topic, Boolean.valueOf(isKey), payload, (Schema)null);
    }

    private DatumReader getDatumReader(Schema writerSchema, Schema readerSchema) {
        if(this.useSpecificAvroReader) {
            if(readerSchema == null) {
                readerSchema = this.getReaderSchema(writerSchema);
            }

            return new SpecificDatumReader(writerSchema, readerSchema);
        } else {
            return readerSchema == null?new GenericDatumReader(writerSchema):new GenericDatumReader(writerSchema, readerSchema);
        }
    }

    private Schema getReaderSchema(Schema writerSchema) {
        Schema readerSchema = (Schema)this.readerSchemaCache.get(writerSchema.getFullName());
        if(readerSchema == null) {
            Class readerClass = SpecificData.get().getClass(writerSchema);
            if(readerClass == null) {
                throw new SerializationException("Could not find class " + writerSchema.getFullName() + " specified in writer\'s schema whilst finding reader\'s schema for a SpecificRecord.");
            }

            try {
                readerSchema = ((SpecificRecord)readerClass.newInstance()).getSchema();
            } catch (InstantiationException var5) {
                throw new SerializationException(writerSchema.getFullName() + " specified by the " + "writers schema could not be instantiated to find the readers schema.");
            } catch (IllegalAccessException var6) {
                throw new SerializationException(writerSchema.getFullName() + " specified by the " + "writers schema is not allowed to be instantiated to find the readers schema.");
            }

            this.readerSchemaCache.put(writerSchema.getFullName(), readerSchema);
        }

        return readerSchema;
    }
}
