package io.simplematter.microservices.common.protocol;

public interface State<T> extends Event<T> {

    T getId();

    long getTimestamp();
}
