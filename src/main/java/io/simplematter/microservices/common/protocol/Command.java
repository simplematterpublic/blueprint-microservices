package io.simplematter.microservices.common.protocol;

import java.io.Serializable;

public interface Command<T> extends Serializable {

    T getId();

    long getTimestamp();
}
