package io.simplematter.microservices.common.protocol;

public interface Snapshot<A, B> extends State<A> {

    B getEventId();
}
