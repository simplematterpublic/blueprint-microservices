package io.simplematter.microservices.common.protocol;

import java.io.Serializable;

public interface Event<T> extends Serializable {

    T getId();

    long getTimestamp();
}
