package io.simplematter.microservices.common.protocol;

public class Envelope<T> {

    private String id;
    private String correlationId;
    private long timeout;
    private T payload;
    private long timestamp;

    public Envelope() {

    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCorrelationId(final String correlationId) {
        this.correlationId = correlationId;
    }

    public void setTimeout(final long timeout) {
        this.timeout = timeout;
    }

    public void setPayload(final T payload) {
        this.payload = payload;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public long getTimeout() {
        return timeout;
    }

    public T getPayload() {
        return payload;
    }

    public String getManifest() {

        return payload.getClass().getName();
    }

    public long getTimestamp() {
        return timestamp;
    }

    public boolean isExpired() {

        return (System.currentTimeMillis() - getTimestamp()) > getTimeout();
    }

}
