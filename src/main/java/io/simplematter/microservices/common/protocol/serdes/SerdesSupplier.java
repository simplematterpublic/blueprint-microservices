package io.simplematter.microservices.common.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import org.apache.kafka.common.serialization.Serde;

public abstract class SerdesSupplier<T> {

    private final String applicationId;
    private final SchemaRegistryClient schemaRegistry;
    private final String registryUrl;

    public SerdesSupplier(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        this.applicationId = applicationId;
        this.schemaRegistry = schemaRegistry;
        this.registryUrl = registryUrl;
    }

    public String getApplicationId() {

        return applicationId;
    }

    public SchemaRegistryClient getSchemaRegistry() {

        return schemaRegistry;
    }

    public String getRegistryUrl() {

        return registryUrl;
    }

    public abstract AvroSerializer<T> getSerializer();

    public abstract AvroDeserializer<T> getDeserializer();

    public abstract Serde<T> getSerdes();
}
