package io.simplematter.microservices.common.protocol;

public interface Converter<A, B> {

    B serialize(A value);

    A deserialize(B value);
}
