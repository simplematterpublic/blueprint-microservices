package io.simplematter.microservices.common.stream;

import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.vertx.core.AbstractVerticle;

import java.util.Objects;
import java.util.Properties;

public abstract class AbstractJob {

    private final String applicationId;
    private final String registryUrl;
    private final SchemaRegistryClient schemaRegistry;
    private final Properties properties;

    public AbstractJob(final Properties properties) {

        this.applicationId = Objects.requireNonNull(properties.getProperty("application.id"));
        this.registryUrl = Objects.requireNonNull(properties.getProperty("schema.registry.url"));
        this.schemaRegistry = new CachedSchemaRegistryClient(registryUrl, 1000);
        this.properties = properties;
    }

    protected String getApplicationId() {

        return applicationId;
    }

    protected String getRegistryUrl() {

        return registryUrl;
    }

    protected SchemaRegistryClient getSchemaRegistry() {

        return schemaRegistry;
    }

    protected Properties getProperties() {

        return properties;
    }

    protected Properties getProperties(final String applicationId) {

        final Properties properties = this.getProperties();
        properties.put("application.id", applicationId);
        return properties;
    }

    public abstract void start();

    public abstract void stop();
}
