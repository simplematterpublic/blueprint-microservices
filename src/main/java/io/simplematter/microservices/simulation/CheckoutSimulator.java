package io.simplematter.microservices.simulation;

import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.checkout.protocol.converter.CommandConverter;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutCommand;
import io.simplematter.microservices.checkout.protocol.internal.command.RequestCheckout;
import io.simplematter.microservices.checkout.protocol.internal.model.PaymentDetail;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import io.simplematter.microservices.common.protocol.Converter;
import io.simplematter.microservices.common.protocol.Event;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.rmi.server.UID;
import java.util.Properties;
import java.util.Random;

public class CheckoutSimulator {

    private static final Random generator = new Random();

    public static RequestCheckout simulate() {

        final int quantity = generator.nextInt(10) + 1;
        final int price = generator.nextInt(1000) + 1;
        final double amount = quantity * price;
        final String sku = "SKU_" + generator.nextInt(10000);
        final String customer = "CUSTOMER_" + generator.nextInt(10000);

        final PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setCardHolder("Name Surname");
        paymentDetail.setCardType("Cart Type");
        paymentDetail.setCardNumber("1234567890");
        paymentDetail.setAmount(amount);
        paymentDetail.setCurrency("EUR");

        final RequestCheckout command = new RequestCheckout();
        command.setId(new UID().toString());
        command.setSku(sku);
        command.setQuantity(quantity);
        command.setPrice(price);
        command.setPaymentDetail(paymentDetail);
        command.setCustomerId(customer);
        command.setTimestamp(System.currentTimeMillis());

        return command;
    }


    public static void main(String[] args) {

        int interval = 10000;
        if (args.length > 0) {
            interval = Integer.parseInt(args[0]);
        }

        final int INDEX = 1;

        final String applicationId = CheckoutSimulator.class.getSimpleName() + "_" + INDEX;

        final Properties props = new Properties();
        props.put("bootstrap.servers", "PLAINTEXT://127.0.0.1:9092");
        //props.put("zookeeper.connect", "localhost:2181")
        props.put("auto.create.topics.enable", "true");
        props.put("linger.ms", "0");
        props.put("timeout.ms", "3000");
        props.put("batch.size", "16384");
        props.put("buffer.memory", "33554432");
        props.put("retries", "1");
        props.put("acks", "all");
        props.put("schema.registry.url", "http://localhost:8081");

        SchemaRegistryClient schemaRegistry = new CachedSchemaRegistryClient("http://localhost:8081", 100);

        final Converter interpreter = new CommandConverter();

        final KafkaProducer<String, Event> producer = new KafkaProducer<>(props,
                new StringSerializer(),
                new AvroSerializer(applicationId, schemaRegistry, interpreter));

        while (true) {
            final RequestCheckout command = simulate();
            final CheckoutCommand envelope = new CheckoutCommand();
            envelope.setId(new UID().toString());
            envelope.setCorrelationId(new UID().toString());
            envelope.setPayload(command);
            envelope.setTimestamp(System.currentTimeMillis());
            final ProducerRecord record = new ProducerRecord("checkout_commands_" + INDEX, 0, null, envelope);
            System.out.println("Sending new RequestCheckout");
            producer.send(record);
            try {
                Thread.sleep(interval);
            } catch (Exception e) {
                System.out.println(e);

            }
        }
    }
}
