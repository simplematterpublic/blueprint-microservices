package io.simplematter.microservices.order.vertx;

import io.simplematter.microservices.order.processor.projector.ProjectorService;
import io.simplematter.microservices.order.protocol.internal.snapshot.Order;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import java.util.List;

public class ProjectorVerticle extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ProjectorService service;

    public ProjectorVerticle(final ProjectorService service) {

        this.service = service;
    }

    @Override
    public void start(final Future<Void> future) throws Exception {

        //client = MongoClient.createShared(vertx, config());
        //service = RatingService.create(vertx, config(), client);
        //ProxyHelper.registerService(RatingService.class, vertx, service, RatingService.ADDRESS);

        final Router router = Router.router(vertx);
        router.get("/api/orders").handler(this::all);
        router.get("/api/orders/ids").handler(this::byIds);
        router.get("/api/orders/:id").handler(this::byId);

        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(config().getInteger("service.http.port", 9001),
                        config().getString("service.http.host", "localhost"), result -> {
                    if (result.succeeded()) {
                        logger.info("API Service deployed successfully.");
                        future.complete();
                    } else {
                        logger.error(result.cause());
                        future.fail(result.cause());
                    }
                });

    }

    @Override
    public void stop(final Future<Void> future) throws Exception {

        //Optional.ofNullable(service).ifPresent(c -> c.close());
        future.complete();
    }

    private void all(final RoutingContext routingContext) {

        final JsonArray jsArray = new JsonArray();
        service.all(10, entities -> {
            entities.forEach(entity -> jsArray.add(toJson(entity)));
            routingContext.response().putHeader("Content-type", "application/json").end(jsArray.encodePrettily());
        });
    }

    private void byId(final RoutingContext routingContext) {

        final String id = routingContext.request().getParam("id");
        service.byId(id, entity -> {
            final JsonObject jsObject = toJson(entity);
            routingContext.response().putHeader("Content-type", "application/json").end(jsObject.encodePrettily());
        });
    }

    private void byIds(final RoutingContext routingContext) {

        final List<String> ids = routingContext.request().params().getAll("id");
        final JsonArray jsArray = new JsonArray();
        service.byIds(ids, entities -> {
            entities.forEach(entity -> jsArray.add(toJson(entity)));
            routingContext.response().putHeader("Content-type", "application/json").end(jsArray.encodePrettily());
        });
    }

    private JsonObject toJson(final Order source) {

        return new JsonObject()
                .put("id", source.getId())
                .put("sku", source.getSku())
                .put("quantity", source.getQuantity())
                .put("price", source.getPrice())
                .put("customerId", source.getCustomerId())
                .put("checkoutId", source.getCheckoutId())
                .put("paymentId", source.getPaymentId())
                .put("status", source.getStatus())
                .put("eventId", source.getEventId())
                .put("eventName", source.getEventName())
                .put("timestamp", source.getTimestamp());
    }

}
