package io.simplematter.microservices.order.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.protocol.serdes.SerdesSupplier;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import io.simplematter.microservices.order.protocol.avro.OrderStateAvro;
import io.simplematter.microservices.order.protocol.converter.StateConverter;
import io.simplematter.microservices.order.protocol.internal.OrderState;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;

public class StateSerdes extends SerdesSupplier<OrderState> {

    public StateSerdes(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        super(applicationId, schemaRegistry, registryUrl);
    }

    @Override
    public AvroSerializer<OrderState> getSerializer() {

        final AvroSerializer<OrderState> serializer = new AvroSerializer(getApplicationId(), getSchemaRegistry(), new StateConverter());

        serializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return serializer;
    }

    @Override
    public AvroDeserializer<OrderState> getDeserializer() {

        final AvroDeserializer<OrderState> deserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(), new StateConverter(), OrderStateAvro.SCHEMA$);

        deserializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return deserializer;
    }

    @Override
    public Serde<OrderState> getSerdes() {

        return Serdes.serdeFrom(getSerializer(), getDeserializer());
    }

}
