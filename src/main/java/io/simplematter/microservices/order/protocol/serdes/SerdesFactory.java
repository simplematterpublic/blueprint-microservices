package io.simplematter.microservices.order.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;

public class SerdesFactory {

    private final String applicationId;
    private final String registryUrl;
    private final SchemaRegistryClient schemaRegistry;

    public SerdesFactory(final String applicationId,
                         final String registryUrl,
                         final SchemaRegistryClient schemaRegistry) {

        this.applicationId = applicationId;
        this.registryUrl = registryUrl;
        this.schemaRegistry = schemaRegistry;
    }

    public final CommandSerdes commandSerdes() {

        return new CommandSerdes(applicationId, schemaRegistry, registryUrl);
    }

    public final EventSerdes eventSerdes() {

        return new EventSerdes(applicationId, schemaRegistry, registryUrl);
    }

    public final StateSerdes stateSerdes() {
        return new StateSerdes(applicationId, schemaRegistry, registryUrl);
    }

    public final SnapshotSerdes snapshotSerdes() {
        return new SnapshotSerdes(applicationId, schemaRegistry, registryUrl);
    }
}
