package io.simplematter.microservices.order.protocol.internal.command;

import io.simplematter.microservices.common.protocol.Command;

public class ConfirmOrder implements Command<String> {

    private String id;
    private String paymentId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getPaymentId() {
        return paymentId;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
