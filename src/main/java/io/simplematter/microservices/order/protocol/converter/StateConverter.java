package io.simplematter.microservices.order.protocol.converter;

import io.simplematter.microservices.common.protocol.Converter;
import io.simplematter.microservices.order.protocol.avro.OrderStateAvro;
import io.simplematter.microservices.order.protocol.avro.state.OrderAvro;
import io.simplematter.microservices.order.protocol.internal.OrderState;
import io.simplematter.microservices.order.protocol.internal.state.Order;

public class StateConverter implements Converter<OrderState, OrderStateAvro> {

    @Override
    public OrderStateAvro serialize(final OrderState source) {

        final OrderStateAvro sink = new OrderStateAvro();
        sink.setId(source.getId());
        sink.setCorrelationId(source.getCorrelationId());
        sink.setManifest(source.getManifest());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() instanceof Order) {
            sink.setPayload(serialize((Order) source.getPayload()));
        }
        return sink;
    }

    @Override
    public OrderState deserialize(final OrderStateAvro source) {

        final OrderState sink = new OrderState();
        sink.setId(source.getId().toString());
        sink.setCorrelationId(source.getCorrelationId().toString());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() != null) {
            sink.setPayload(deserialize(source.getPayload()));
        }
        return sink;
    }

    private OrderAvro serialize(final Order source) {

        final OrderAvro sink = new OrderAvro();
        sink.setId(source.getId());
        sink.setSku(source.getSku());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setCustomerId(source.getCustomerId());
        sink.setCheckoutId(source.getCheckoutId());
        sink.setPaymentId(source.getPaymentId());
        sink.setStatus(source.getStatus());
        sink.setEventId(source.getEventId());
        sink.setOffset(source.getOffset());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private Order deserialize(final OrderAvro source) {

        final Order sink = new Order();
        sink.setId(source.getId().toString());
        sink.setSku(source.getSku().toString());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setCustomerId(source.getCustomerId().toString());
        sink.setCheckoutId(source.getCheckoutId().toString());
        sink.setPaymentId(source.getPaymentId() == null ? null : source.getPaymentId().toString());
        sink.setStatus(source.getStatus().toString());
        sink.setEventId(source.getEventId().toString());
        sink.setOffset(source.getOffset());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }
}
