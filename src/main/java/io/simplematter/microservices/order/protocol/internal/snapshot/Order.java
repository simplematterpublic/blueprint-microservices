package io.simplematter.microservices.order.protocol.internal.snapshot;

import io.simplematter.microservices.common.protocol.Snapshot;

public class Order implements Snapshot<String, String> {

    private String id;
    private String sku;
    private int quantity;
    private double price;
    private String customerId;
    private String checkoutId;
    private String paymentId;
    private String status;
    private String eventId;
    private String eventName;
    private long timestamp;

    public static Order create(final io.simplematter.microservices.order.protocol.internal.state.Order source, final String eventName) {

        final Order sink = new Order();
        sink.setId(source.getId().toString());
        sink.setSku(source.getSku().toString());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setCustomerId(source.getCustomerId().toString());
        sink.setCheckoutId(source.getCheckoutId().toString());
        sink.setPaymentId(source.getPaymentId() == null ? null : source.getPaymentId().toString());
        sink.setStatus(source.getStatus().toString());
        sink.setEventId(source.getEventId().toString());
        sink.setEventName(eventName);
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setCheckoutId(String checkoutId) {
        this.checkoutId = checkoutId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getSku() {
        return sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getCheckoutId() {
        return checkoutId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String getEventId() {
        return eventId;
    }

    public String getEventName() {
        return eventName;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
