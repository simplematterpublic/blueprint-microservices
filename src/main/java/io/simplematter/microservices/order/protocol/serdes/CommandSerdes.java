package io.simplematter.microservices.order.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.protocol.serdes.SerdesSupplier;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import io.simplematter.microservices.order.protocol.converter.CommandConverter;
import io.simplematter.microservices.order.protocol.avro.OrderCommandAvro;
import io.simplematter.microservices.order.protocol.internal.OrderCommand;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;

public class CommandSerdes extends SerdesSupplier<OrderCommand> {

    public CommandSerdes(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        super(applicationId, schemaRegistry, registryUrl);
    }

    @Override
    public AvroSerializer<OrderCommand> getSerializer() {

        final AvroSerializer<OrderCommand> serializer = new AvroSerializer(getApplicationId(), getSchemaRegistry(), new CommandConverter());

        serializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return serializer;
    }

    @Override
    public AvroDeserializer<OrderCommand> getDeserializer() {

        final AvroDeserializer<OrderCommand> deserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(), new CommandConverter(), OrderCommandAvro.SCHEMA$);

        deserializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return deserializer;
    }

    @Override
    public Serde<OrderCommand> getSerdes() {

        return Serdes.serdeFrom(getSerializer(), getDeserializer());
    }

}
