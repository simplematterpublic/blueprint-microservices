package io.simplematter.microservices.order.protocol.internal.command;

import io.simplematter.microservices.common.protocol.Command;

public class RejectOrder implements Command<String> {

    private String id;
    private String reason;
    private String paymentId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setPaymentId(String checkoutId) {
        this.paymentId = paymentId;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public String getReason() {
        return reason;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
