package io.simplematter.microservices.order.protocol.internal.event;

import io.simplematter.microservices.order.protocol.internal.model.PaymentDetail;
import io.simplematter.microservices.common.protocol.Event;

public class OrderValidated implements Event<String> {

    private String id;
    private String sku;
    private int quantity;
    private double price;
    private String customerId;
    private PaymentDetail paymentDetail;
    private String checkoutId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setPaymentDetail(PaymentDetail paymentDetail) {
        this.paymentDetail = paymentDetail;
    }

    public void setCheckoutId(String checkoutId) {
        this.checkoutId = checkoutId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getSku() {
        return sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getCustomerId() {
        return customerId;
    }

    public PaymentDetail getPaymentDetail() {
        return paymentDetail;
    }

    public String getCheckoutId() {
        return checkoutId;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
