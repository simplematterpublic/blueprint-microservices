package io.simplematter.microservices.order.protocol.internal.event;

import io.simplematter.microservices.common.protocol.Event;

public class OrderConfirmed implements Event<String> {

    private String id;
    private String checkoutId;
    private String paymentId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setCheckoutId(String checkoutId) {
        this.checkoutId = checkoutId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getCheckoutId() {
        return checkoutId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
