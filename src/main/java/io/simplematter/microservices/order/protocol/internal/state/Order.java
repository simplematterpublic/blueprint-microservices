package io.simplematter.microservices.order.protocol.internal.state;

import io.simplematter.microservices.common.protocol.State;
import io.simplematter.microservices.order.protocol.internal.event.OrderConfirmed;
import io.simplematter.microservices.order.protocol.internal.event.OrderRejected;
import io.simplematter.microservices.order.protocol.internal.event.OrderValidated;

public class Order implements State<String> {

    public static final String VALIDATED = "VALIDATED";
    public static final String CONFIRMED = "CONFIRMED";
    public static final String REJECTED = "REJECTED";

    private String id;
    private String sku;
    private int quantity;
    private double price;
    private String customerId;
    private String checkoutId;
    private String paymentId;
    private String status;
    private String eventId;
    private long offset;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setCheckoutId(String checkoutId) {
        this.checkoutId = checkoutId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getSku() {
        return sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getCheckoutId() {
        return checkoutId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public String getStatus() {
        return status;
    }

    public String getEventId() {
        return eventId;
    }

    public long getOffset() {
        return offset;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    public static Order create(final OrderValidated event, final long offset) {

        final Order state = new Order();
        state.setId(event.getId());
        state.setSku(event.getSku());
        state.setQuantity(event.getQuantity());
        state.setPrice(event.getPrice());
        state.setCustomerId(event.getCustomerId());
        state.setCheckoutId(event.getCheckoutId());
        state.setStatus(Order.VALIDATED);
        state.setEventId(event.getId());
        state.setOffset(offset);
        state.setTimestamp(System.currentTimeMillis());
        return state;
    }

    public Order update(final OrderConfirmed event, final long offset) {

        setPaymentId(event.getPaymentId());
        setStatus(Order.CONFIRMED);
        setEventId(event.getId());
        setOffset(offset);
        setTimestamp(System.currentTimeMillis());
        return this;
    }

    public Order update(final OrderRejected event, final long offset) {

        setStatus(Order.REJECTED);
        setEventId(event.getId());
        setOffset(offset);
        setTimestamp(System.currentTimeMillis());
        return this;
    }
}
