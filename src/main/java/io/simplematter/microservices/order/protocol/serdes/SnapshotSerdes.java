package io.simplematter.microservices.order.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.protocol.serdes.SerdesSupplier;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import io.simplematter.microservices.order.protocol.avro.OrderSnapshotAvro;
import io.simplematter.microservices.order.protocol.converter.SnapshotConverter;
import io.simplematter.microservices.order.protocol.internal.OrderSnapshot;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;

public class SnapshotSerdes extends SerdesSupplier<OrderSnapshot> {

    public SnapshotSerdes(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        super(applicationId, schemaRegistry, registryUrl);
    }

    @Override
    public AvroSerializer<OrderSnapshot> getSerializer() {

        final AvroSerializer<OrderSnapshot> serializer = new AvroSerializer(getApplicationId(), getSchemaRegistry(), new SnapshotConverter());

        serializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return serializer;
    }

    @Override
    public AvroDeserializer<OrderSnapshot> getDeserializer() {

        final AvroDeserializer<OrderSnapshot> deserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(), new SnapshotConverter(), OrderSnapshotAvro.SCHEMA$);

        deserializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return deserializer;
    }

    @Override
    public Serde<OrderSnapshot> getSerdes() {

        return Serdes.serdeFrom(getSerializer(), getDeserializer());
    }

}
