package io.simplematter.microservices.order;

import io.simplematter.microservices.order.vertx.StartupVerticle;
import io.vertx.core.Vertx;

public class OrderService {

    public static void main(String args[]) {

        final Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new StartupVerticle());
    }
}
