package io.simplematter.microservices.order.processor.projector;

import io.simplematter.microservices.common.projection.Store;
import io.simplematter.microservices.common.stream.AbstractJob;
import io.simplematter.microservices.order.protocol.internal.snapshot.Order;

import java.util.Properties;

public class ProjectorRunner extends AbstractJob {

    private final OrderProjector projector;

    public ProjectorRunner(final Properties properties, final Store<String, Order> store) {

        super(properties);

        projector = new OrderProjector(getProperties(), store);
    }

    @Override
    public void start() {

        projector.start();
    }

    @Override
    public void stop() {

        projector.stop();
    }
}
