package io.simplematter.microservices.order.processor.adapter;

import io.simplematter.microservices.common.protocol.Command;
import io.simplematter.microservices.common.protocol.Event;
import io.simplematter.microservices.order.protocol.internal.OrderCommand;
import io.simplematter.microservices.order.protocol.internal.command.ConfirmOrder;
import io.simplematter.microservices.order.protocol.internal.command.RejectOrder;
import io.simplematter.microservices.payment.protocol.internal.PaymentEvent;
import io.simplematter.microservices.payment.protocol.internal.event.PaymentProcessed;
import io.simplematter.microservices.payment.protocol.internal.event.PaymentRejected;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.streams.processor.AbstractProcessor;

public class PaymentAdapter extends AbstractProcessor<String, PaymentEvent> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void process(final String key, final PaymentEvent paymentEnvelope) {

        try {
            doProcess(key, paymentEnvelope);
        } catch (Exception e) {
            logger.error(e);
        } finally {
            context().commit();
        }
    }

    private void doProcess(final String key, final PaymentEvent paymentEnvelope) {

        final OrderCommand orderCommand = new OrderCommand(paymentEnvelope.getId(), paymentEnvelope.getCorrelationId());
        final Event event = paymentEnvelope.getPayload();
        logger.info("AdapterProcessor received message: " + paymentEnvelope.getPayload().getClass().getName());
        if (event instanceof PaymentProcessed) {
            orderCommand.setPayload(convert((PaymentProcessed) event));
            context().forward(key, orderCommand);
        } else if (event instanceof PaymentRejected) {
            orderCommand.setPayload(convert((PaymentRejected) event));
            context().forward(key, orderCommand);
        } else {
            if (logger.isDebugEnabled()) {
                logger.warn("AdapterProcessor skipping unhandled message: " + paymentEnvelope.getPayload().getClass().getName());
            }
        }
    }

    private Command convert(final PaymentProcessed event) {

        final ConfirmOrder command = new ConfirmOrder();
        command.setId(event.getOrderId());
        command.setPaymentId(event.getId());
        command.setTimestamp(event.getTimestamp());
        return command;
    }

    private Command convert(final PaymentRejected event) {

        final RejectOrder command = new RejectOrder();
        command.setId(event.getOrderId());
        command.setReason(event.getReason());
        command.setTimestamp(event.getTimestamp());
        return command;
    }
}
