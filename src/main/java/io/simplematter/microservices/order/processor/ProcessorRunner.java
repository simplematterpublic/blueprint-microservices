package io.simplematter.microservices.order.processor;

import io.simplematter.microservices.order.StaticConfigs;
import io.simplematter.microservices.order.protocol.serdes.*;
import io.simplematter.microservices.common.stream.AbstractJob;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.StateStoreSupplier;
import org.apache.kafka.streams.processor.TopologyBuilder;
import org.apache.kafka.streams.state.Stores;

import java.util.Properties;

public class ProcessorRunner extends AbstractJob {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private KafkaStreams kafkaStreams;

    public ProcessorRunner(final Properties properties) {

        super(properties);
    }

    @Override
    public void start() {

        logger.info("Starting " + getClass().getSimpleName());

        final String applicationId = getApplicationId();

        logger.info("Registering applicationId: " + applicationId);

        final SerdesFactory serdesFactory = new SerdesFactory(getApplicationId(), getRegistryUrl(), getSchemaRegistry());

        final CommandSerdes commandSerdes = serdesFactory.commandSerdes();
        final EventSerdes eventSerdes = serdesFactory.eventSerdes();
        final StateSerdes stateSerdes = serdesFactory.stateSerdes();
        final SnapshotSerdes snapshotSerdes = serdesFactory.snapshotSerdes();

        final StateStoreSupplier store = Stores.create(StaticConfigs.STATE_STORE).withStringKeys()
                .withValues(stateSerdes.getSerdes())
                .persistent()
                .build();

        final TopologyBuilder topologyBuilder = new TopologyBuilder();

        topologyBuilder
                .addSource(StaticConfigs.COMMAND_SOURCE, new StringDeserializer(), commandSerdes.getDeserializer(), StaticConfigs.COMMAND_TOPIC)
                .addProcessor(StaticConfigs.COMMAND_PROCESSOR, new OrderProcessor(store.name(), StaticConfigs.ROUTING_PROCESSOR, StaticConfigs.ERROR_SINK), StaticConfigs.COMMAND_SOURCE)
                .addStateStore(store, StaticConfigs.COMMAND_PROCESSOR)
                .addSink(StaticConfigs.ERROR_SINK, StaticConfigs.ERROR_TOPIC, new StringSerializer(), eventSerdes.getSerializer(), StaticConfigs.COMMAND_PROCESSOR)

                .addProcessor(StaticConfigs.ROUTING_PROCESSOR, new OrderDispatcher(store.name(), StaticConfigs.EVENT_SINK, StaticConfigs.SNAPSHOT_SINK), StaticConfigs.COMMAND_PROCESSOR)
                .connectProcessorAndStateStores(StaticConfigs.ROUTING_PROCESSOR, store.name())
                .addSink(StaticConfigs.SNAPSHOT_SINK, StaticConfigs.SNAPSHOT_TOPIC, new StringSerializer(), snapshotSerdes.getSerializer(), StaticConfigs.ROUTING_PROCESSOR)
                .addSink(StaticConfigs.EVENT_SINK, StaticConfigs.EVENT_TOPIC, new StringSerializer(), eventSerdes.getSerializer(), StaticConfigs.ROUTING_PROCESSOR);

        final StreamsConfig streamsConfig = new StreamsConfig(getProperties());

        kafkaStreams = new KafkaStreams(topologyBuilder, streamsConfig);

        kafkaStreams.start();

        logger.info(getClass().getSimpleName() + " Started");
    }

    @Override
    public void stop() {

        logger.info("Stopping " + getClass().getSimpleName());

        kafkaStreams.close();

        logger.info(getClass().getSimpleName() + " Stopped");
    }
}
