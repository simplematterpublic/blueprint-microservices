package io.simplematter.microservices.order.processor.adapter;

import io.simplematter.microservices.checkout.protocol.avro.CheckoutEventAvro;
import io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro;
import io.simplematter.microservices.order.StaticConfigs;
import io.simplematter.microservices.order.protocol.serdes.CommandSerdes;
import io.simplematter.microservices.order.protocol.serdes.SerdesFactory;
import io.simplematter.microservices.common.protocol.Event;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.stream.AbstractJob;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.TopologyBuilder;

import java.util.HashMap;
import java.util.Properties;

public class AdapterRunner extends AbstractJob {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private KafkaStreams kafkaStreams;

    public AdapterRunner(final Properties properties) {

        super(properties);
    }

    @Override
    public void start() {

        logger.info("Starting " + getClass().getSimpleName());

        final String applicationId = getApplicationId();

        logger.info("Registering applicationId: " + applicationId);

        final SerdesFactory serdesFactory = new SerdesFactory(getApplicationId(), getRegistryUrl(), getSchemaRegistry());

        final AvroDeserializer<Event> checkoutDeserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(),
                new io.simplematter.microservices.checkout.protocol.converter.EventConverter(),
                CheckoutEventAvro.SCHEMA$);

        checkoutDeserializer.configure(new HashMap() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);


        final AvroDeserializer<Event> paymentDeserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(),
                new io.simplematter.microservices.payment.protocol.converter.EventConverter(),
                PaymentEventAvro.SCHEMA$);

        paymentDeserializer.configure(new HashMap() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        final CommandSerdes commandSerdes = serdesFactory.commandSerdes();

        final TopologyBuilder topologyBuilder = new TopologyBuilder();

        topologyBuilder
                .addSource("CHECKOUT_EVENT_SOURCE", new StringDeserializer(), checkoutDeserializer, "checkout_events_" + StaticConfigs.INDEX)
                .addProcessor("CHECKOUT_ADAPTER_PROCESSOR", CheckoutAdapter::new, "CHECKOUT_EVENT_SOURCE")

                .addSource("PAYMENT_EVENT_SOURCE", new StringDeserializer(), paymentDeserializer, "payment_events_" + StaticConfigs.INDEX)
                .addProcessor("PAYMENT_ADAPTER_PROCESSOR", PaymentAdapter::new, "PAYMENT_EVENT_SOURCE")

                .addSink("COMMAND_SINK", StaticConfigs.COMMAND_TOPIC, new StringSerializer(),
                        commandSerdes.getSerializer(), "CHECKOUT_ADAPTER_PROCESSOR", "PAYMENT_ADAPTER_PROCESSOR");

        final StreamsConfig streamsConfig = new StreamsConfig(getProperties());

        kafkaStreams = new KafkaStreams(topologyBuilder, streamsConfig);

        kafkaStreams.start();

        logger.info(getClass().getSimpleName() + " Started");
    }

    @Override
    public void stop() {

        logger.info("Stopping " + getClass().getSimpleName());

        kafkaStreams.close();

        logger.info(getClass().getSimpleName() + " Stopped");
    }
}
