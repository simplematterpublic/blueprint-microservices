package io.simplematter.microservices.order.processor.adapter;

import io.simplematter.microservices.checkout.protocol.internal.CheckoutEvent;
import io.simplematter.microservices.checkout.protocol.internal.event.CheckoutRequested;
import io.simplematter.microservices.common.protocol.Command;
import io.simplematter.microservices.common.protocol.Event;
import io.simplematter.microservices.order.protocol.internal.OrderCommand;
import io.simplematter.microservices.order.protocol.internal.command.ValidateOrder;
import io.simplematter.microservices.order.protocol.internal.model.PaymentDetail;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.streams.processor.AbstractProcessor;

import java.rmi.server.UID;

public class CheckoutAdapter extends AbstractProcessor<String, CheckoutEvent> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void process(final String key, final CheckoutEvent checkoutEnvelope) {

        try {
            doProcess(key, checkoutEnvelope);
        } catch (Exception e) {
            logger.error(e);
        } finally {
            context().commit();
        }
    }

    private void doProcess(final String key, final CheckoutEvent checkoutEnvelope) {

        final OrderCommand orderCommand = new OrderCommand(checkoutEnvelope.getId(), checkoutEnvelope.getCorrelationId());
        final Event event = checkoutEnvelope.getPayload();
        logger.info("AdapterProcessor received message: " + checkoutEnvelope.getPayload().getClass().getName());
        if (event instanceof CheckoutRequested) {
            orderCommand.setPayload(convert((CheckoutRequested) event));
            context().forward(key, orderCommand);
        } else {
            if (logger.isDebugEnabled()) {
                logger.warn("AdapterProcessor skipping unhandled message: " + checkoutEnvelope.getPayload().getClass().getName());
            }
        }
    }

    private Command convert(final CheckoutRequested event) {

        final PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setCardHolder(event.getPaymentDetail().getCardHolder());
        paymentDetail.setCardType(event.getPaymentDetail().getCardType());
        paymentDetail.setCardNumber(event.getPaymentDetail().getCardNumber());
        paymentDetail.setAmount(event.getPaymentDetail().getAmount());
        paymentDetail.setCurrency(event.getPaymentDetail().getCurrency());

        final ValidateOrder command = new ValidateOrder();
        command.setId(new UID().toString());
        command.setSku(event.getSku());
        command.setQuantity(event.getQuantity());
        command.setPrice(event.getPrice());
        command.setCustomerId(event.getCustomerId());
        command.setCheckoutId(event.getId());
        command.setPaymentDetail(paymentDetail);
        command.setTimestamp(event.getTimestamp());
        return command;
    }
}
