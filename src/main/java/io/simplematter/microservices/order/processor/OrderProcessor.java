package io.simplematter.microservices.order.processor;

import io.simplematter.microservices.checkout.protocol.internal.event.BusinessRejected;
import io.simplematter.microservices.common.protocol.Command;
import io.simplematter.microservices.order.protocol.internal.OrderCommand;
import io.simplematter.microservices.order.protocol.internal.OrderEvent;
import io.simplematter.microservices.order.protocol.internal.OrderState;
import io.simplematter.microservices.common.kafka.streams.processor.StoreProcessor;
import io.simplematter.microservices.order.protocol.internal.command.ConfirmOrder;
import io.simplematter.microservices.order.protocol.internal.command.RejectOrder;
import io.simplematter.microservices.order.protocol.internal.command.ValidateOrder;
import io.simplematter.microservices.order.protocol.internal.event.OrderConfirmed;
import io.simplematter.microservices.order.protocol.internal.event.OrderRejected;
import io.simplematter.microservices.order.protocol.internal.event.OrderValidated;
import io.simplematter.microservices.order.protocol.internal.state.Order;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorSupplier;

public class OrderProcessor implements ProcessorSupplier<String, OrderCommand> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String storeName;
    private final String eventDestination;
    private final String errorDestination;

    public OrderProcessor(final String storeName, final String eventDestination, final String errorDestination) {

        this.storeName = storeName;
        this.eventDestination = eventDestination;
        this.errorDestination = errorDestination;
    }

    public Processor<String, OrderCommand> get() {

        return new StoreProcessor<String, OrderCommand, OrderState>() {

            public String getStoreName() {

                return storeName;
            }

            public String getEventDestination() {

                return eventDestination;
            }

            public String getErrorDestination() {

                return errorDestination;
            }

            @Override
            public void process(final String key, final OrderCommand value) {

                try {
                    //if (!orderCommand.isExpired()) {
                    doProcess(key, value);
                    //} else {
                    // TODO Handle
                    //}
                } catch (Exception e) {
                    logger.error(e);
                } finally {
                    context().commit();
                }

            }

            private void onException(final String key, final OrderCommand orderCommand, final Throwable throwable) {

                final OrderEvent orderEvent = new OrderEvent(orderCommand.getId(), orderCommand.getCorrelationId());
                final Order state = getState(orderCommand.getPayload().getId());
                if (state != null) {
                    final OrderState orderState = new OrderState(orderCommand.getId(), orderCommand.getCorrelationId());
                    state.setStatus(Order.REJECTED);
                    orderState.setPayload(state);
                    getStore().put(state.getId(), orderState);
                }
                final BusinessRejected businessRejected = new BusinessRejected();
                businessRejected.setId(orderCommand.getId());
                businessRejected.setOrigId(orderCommand.getId());
                businessRejected.setReason(throwable.getMessage());
                businessRejected.setTimestamp(System.currentTimeMillis());
                context().forward(key, orderEvent, getErrorDestination());
            }

            private void doProcess(final String key, final OrderCommand orderCommand) {

                final OrderState stateEnvelope = new OrderState(orderCommand.getId(), orderCommand.getCorrelationId());
                final OrderEvent eventEnvelope = new OrderEvent(orderCommand.getId(), orderCommand.getCorrelationId());
                final Command command = orderCommand.getPayload();
                logger.info("ServiceProcessor received message: " + command.getClass().getName());
                if (command instanceof ValidateOrder) {
                    final Order currentState = getState(((ValidateOrder) command).getId());
                    if (currentState == null || currentState.getStatus().equals(Order.VALIDATED)) {
                        final OrderValidated event = transform((ValidateOrder) command);
                        eventEnvelope.setPayload(event);
                        final Order state = Order.create(event, context().offset());
                        stateEnvelope.setPayload(state);
                        getStore().put(state.getId(), stateEnvelope);
                        context().forward(state.getId(), eventEnvelope, getEventDestination());
                    } else {
                        // TODO: Send business reject message
                        throw new RuntimeException("Invalid state " + currentState.getStatus());
                    }
                } else if (command instanceof ConfirmOrder) {
                    final Order currentState = getState(((ConfirmOrder) command).getId());
                    if (currentState != null && (currentState.getStatus().equals(Order.VALIDATED) ||
                            currentState.getStatus().equals(Order.CONFIRMED))) {
                        final OrderConfirmed event = transform(currentState, (ConfirmOrder) command);
                        eventEnvelope.setPayload(event);
                        final Order state = currentState.update(event, context().offset());
                        stateEnvelope.setPayload(state);
                        getStore().put(state.getId(), stateEnvelope);
                        context().forward(state.getId(), eventEnvelope, getEventDestination());
                    } else {
                        // TODO: Send business reject message
                        throw new RuntimeException("Invalid state " + currentState.getStatus());
                    }
                } else if (command instanceof RejectOrder) {
                    final Order currentState = getState(((RejectOrder) command).getId());
                    if (currentState != null) {
                        final OrderRejected event = transform(currentState, (RejectOrder) command);
                        eventEnvelope.setPayload(event);
                        final Order state = currentState.update(event, context().offset());
                        stateEnvelope.setPayload(state);
                        getStore().put(state.getId(), stateEnvelope);
                        context().forward(state.getId(), eventEnvelope, getEventDestination());
                    } else {
                        // TODO: Send business reject message
                        throw new RuntimeException("State not found");
                    }
                } else {
                    throw new RuntimeException("Received unknown message: " + command.getClass().getName());
                }
            }

            private OrderValidated transform(final ValidateOrder command) {

                final OrderValidated event = new OrderValidated();
                event.setId(command.getId());
                event.setSku(command.getSku());
                event.setQuantity(command.getQuantity());
                event.setPrice(command.getPrice());
                event.setPaymentDetail(command.getPaymentDetail());
                event.setCustomerId(command.getCustomerId());
                event.setCheckoutId(command.getCheckoutId());
                event.setTimestamp(System.currentTimeMillis());
                return event;
            }

            private OrderConfirmed transform(final Order state, final ConfirmOrder command) {

                final OrderConfirmed event = new OrderConfirmed();
                event.setId(command.getId());
                event.setCheckoutId(state.getCheckoutId());
                event.setPaymentId(command.getPaymentId());
                event.setTimestamp(System.currentTimeMillis());
                return event;
            }

            private OrderRejected transform(final Order state, final RejectOrder command) {

                final OrderRejected event = new OrderRejected();
                event.setId(command.getId());
                event.setReason(command.getReason());
                event.setCheckoutId(state.getCheckoutId());
                event.setTimestamp(System.currentTimeMillis());
                return event;
            }
        };
    }
}
