package io.simplematter.microservices.order.processor.projector;

import io.simplematter.microservices.order.protocol.internal.snapshot.Order;
import io.vertx.core.Handler;

import java.util.List;

public class ProjectorService {

    private final ProjectorStore store;

    public ProjectorService(final ProjectorStore store) {

        this.store = store;
    }

    public void byId(final String id, final Handler<Order> handler) {

        handler.handle(store.byId(id));
    }

    public void byIds(final List<String> ids, final Handler<List<Order>> handler) {

        handler.handle(store.byIds(ids));
    }

    public void all(final int limit, final Handler<List<Order>> handler) {

        handler.handle(store.all(limit));
    }
}
