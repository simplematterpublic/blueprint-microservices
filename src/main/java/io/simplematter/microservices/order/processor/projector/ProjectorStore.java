package io.simplematter.microservices.order.processor.projector;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.model.MapObject;
import com.rethinkdb.net.Connection;
import io.simplematter.microservices.common.projection.AbstractStore;
import io.simplematter.microservices.common.util.TypeUtil;
import io.simplematter.microservices.order.protocol.internal.snapshot.Order;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.HashMap;

public class ProjectorStore extends AbstractStore<String, Order> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public static final RethinkDB r = RethinkDB.r;

    public ProjectorStore(final Connection connection, final String table) {

        super(connection, table);
    }

    @Override
    protected Order convert(final HashMap source) {

        final Order sink = new Order();
        sink.setId((String) source.get("id"));
        sink.setSku((String) source.get("sku"));
        sink.setQuantity(TypeUtil.toInteger(source.get("quantity")));
        sink.setPrice(TypeUtil.toDouble(source.get("price")));
        sink.setCustomerId((String) source.get("customerId"));
        sink.setCheckoutId((String) source.get("checkoutId"));
        sink.setPaymentId((String) source.get("paymentId"));
        sink.setStatus((String) source.get("status"));
        sink.setEventId((String) source.get("eventId"));
        sink.setEventName((String) source.get("eventName"));
        sink.setTimestamp((Long) source.get("timestamp"));
        return sink;
    }

    @Override
    protected MapObject convert(final Order source) {

        return r.hashMap("id", source.getId())
                .with("sku", source.getSku())
                .with("quantity", source.getQuantity())
                .with("price", source.getPrice())
                .with("customerId", source.getCustomerId())
                .with("checkoutId", source.getCheckoutId())
                .with("paymentId", source.getPaymentId())
                .with("status", source.getStatus())
                .with("eventId", source.getEventId())
                .with("eventName", source.getEventName())
                .with("timestamp", source.getTimestamp());
    }
}
