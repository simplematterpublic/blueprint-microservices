package io.simplematter.microservices.payment.protocol.internal.event;

import io.simplematter.microservices.common.protocol.Event;

public class PaymentRejected implements Event<String> {

    private String id;
    private String reason;
    private String orderId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getReason() {
        return reason;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

}
