package io.simplematter.microservices.payment.protocol.internal.state;

import io.simplematter.microservices.common.protocol.State;
import io.simplematter.microservices.payment.protocol.internal.event.PaymentProcessed;
import io.simplematter.microservices.payment.protocol.internal.event.PaymentRejected;

public class Payment implements State<String> {

    public static final String PROCESSED = "PROCESSED";
    public static final String REJECTED = "REJECTED";

    private String id;
    private String cardHolder;
    private String cardType;
    private String cardNumber;
    private Double amount;
    private String currency;
    private String orderId;
    private String status;
    private String eventId;
    private long offset;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public String getCardType() {
        return cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getStatus() {
        return status;
    }

    public String getEventId() {
        return eventId;
    }

    public long getOffset() {
        return offset;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    public static Payment create(final PaymentProcessed event, long offset) {

        final Payment state = new Payment();
        state.setId(event.getId());
        state.setCardHolder(event.getCardHolder());
        state.setCardType(event.getCardType());
        state.setCardNumber(event.getCardNumber());
        state.setAmount(event.getAmount());
        state.setCurrency(event.getCurrency());
        state.setOrderId(event.getOrderId());
        state.setStatus(Payment.PROCESSED);
        state.setEventId(event.getId());
        state.setOffset(offset);
        state.setTimestamp(System.currentTimeMillis());
        return state;
    }

    public Payment update(final PaymentRejected event, long offset) {

        setStatus(Payment.REJECTED);
        setEventId(event.getId());
        setOffset(offset);
        setTimestamp(System.currentTimeMillis());
        return this;
    }
}
