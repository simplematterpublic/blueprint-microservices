package io.simplematter.microservices.payment.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.protocol.serdes.SerdesSupplier;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import io.simplematter.microservices.payment.protocol.converter.StateConverter;
import io.simplematter.microservices.payment.protocol.avro.PaymentStateAvro;
import io.simplematter.microservices.payment.protocol.internal.PaymentState;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;

public class StateSerdes extends SerdesSupplier<PaymentState> {


    public StateSerdes(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        super(applicationId, schemaRegistry, registryUrl);
    }

    @Override
    public AvroSerializer<PaymentState> getSerializer() {

        final AvroSerializer<PaymentState> serializer = new AvroSerializer(getApplicationId(), getSchemaRegistry(), new StateConverter());

        serializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return serializer;
    }

    @Override
    public AvroDeserializer<PaymentState> getDeserializer() {

        final AvroDeserializer<PaymentState> deserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(), new StateConverter(), PaymentStateAvro.SCHEMA$);

        deserializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return deserializer;
    }

    @Override
    public Serde<PaymentState> getSerdes() {

        return Serdes.serdeFrom(getSerializer(), getDeserializer());
    }

}
