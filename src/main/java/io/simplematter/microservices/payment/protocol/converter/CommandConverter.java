package io.simplematter.microservices.payment.protocol.converter;

import io.simplematter.microservices.common.protocol.Converter;
import io.simplematter.microservices.payment.protocol.avro.PaymentCommandAvro;
import io.simplematter.microservices.payment.protocol.avro.command.ProcessPaymentAvro;
import io.simplematter.microservices.payment.protocol.internal.PaymentCommand;
import io.simplematter.microservices.payment.protocol.internal.command.ProcessPayment;

public class CommandConverter implements Converter<PaymentCommand, PaymentCommandAvro> {

    @Override
    public PaymentCommandAvro serialize(final PaymentCommand source) {

        final PaymentCommandAvro sink = new PaymentCommandAvro();
        sink.setId(source.getId());
        sink.setCorrelationId(source.getCorrelationId());
        sink.setManifest(source.getManifest());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() instanceof ProcessPayment) {
            sink.setProcessPayment(convert((ProcessPayment) source.getPayload()));
        }
        return sink;

    }

    @Override
    public PaymentCommand deserialize(final PaymentCommandAvro source) {

        final PaymentCommand sink = new PaymentCommand();
        sink.setId(source.getId().toString());
        sink.setCorrelationId(source.getCorrelationId().toString());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getProcessPayment() != null) {
            sink.setPayload(convert(source.getProcessPayment()));
        }
        return sink;
    }

    private ProcessPaymentAvro convert(final ProcessPayment source) {

        final ProcessPaymentAvro sink = new ProcessPaymentAvro();
        sink.setId(source.getId());
        sink.setCardHolder(source.getCardHolder());
        sink.setCardType(source.getCardType());
        sink.setCardNumber(source.getCardNumber());
        sink.setAmount(source.getAmount());
        sink.setCurrency(source.getCurrency());
        sink.setOrderId(source.getOrderId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private ProcessPayment convert(final ProcessPaymentAvro source) {

        final ProcessPayment sink = new ProcessPayment();
        sink.setId(source.getId().toString());
        sink.setCardHolder(source.getCardHolder().toString());
        sink.setCardType(source.getCardType().toString());
        sink.setCardNumber(source.getCardNumber().toString());
        sink.setAmount(source.getAmount());
        sink.setCurrency(source.getCurrency().toString());
        sink.setOrderId(source.getOrderId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }
}
