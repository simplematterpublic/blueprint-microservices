package io.simplematter.microservices.payment.protocol.converter;

import io.simplematter.microservices.common.protocol.Converter;
import io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro;
import io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro;
import io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro;
import io.simplematter.microservices.payment.protocol.internal.PaymentEvent;
import io.simplematter.microservices.payment.protocol.internal.event.PaymentProcessed;
import io.simplematter.microservices.payment.protocol.internal.event.PaymentRejected;

public class EventConverter implements Converter<PaymentEvent, PaymentEventAvro> {

    @Override
    public PaymentEventAvro serialize(final PaymentEvent source) {

        final PaymentEventAvro sink = new PaymentEventAvro();
        sink.setId(source.getId());
        sink.setCorrelationId(source.getCorrelationId());
        sink.setManifest(source.getManifest());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() instanceof PaymentProcessed) {
            sink.setPaymentProcessed(convert((PaymentProcessed) source.getPayload()));
        } else if (source.getPayload() instanceof PaymentRejected) {
            sink.setPaymentRejected(convert((PaymentRejected) source.getPayload()));
        }
        return sink;

    }

    @Override
    public PaymentEvent deserialize(final PaymentEventAvro source) {

        final PaymentEvent sink = new PaymentEvent();
        sink.setId(source.getId().toString());
        sink.setCorrelationId(source.getCorrelationId().toString());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPaymentProcessed() != null) {
            sink.setPayload(convert(source.getPaymentProcessed()));
        } else if (source.getPaymentRejected() != null) {
            sink.setPayload(convert(source.getPaymentRejected()));
        }
        return sink;
    }

    private PaymentProcessedAvro convert(final PaymentProcessed source) {

        final PaymentProcessedAvro sink = new PaymentProcessedAvro();
        sink.setId(source.getId());
        sink.setCardHolder(source.getCardHolder());
        sink.setCardType(source.getCardType());
        sink.setCardNumber(source.getCardNumber());
        sink.setAmount(source.getAmount());
        sink.setCurrency(source.getCurrency());
        sink.setOrderId(source.getOrderId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private PaymentProcessed convert(final PaymentProcessedAvro source) {

        final PaymentProcessed sink = new PaymentProcessed();
        sink.setId(source.getId().toString());
        sink.setCardHolder(source.getCardHolder().toString());
        sink.setCardType(source.getCardType().toString());
        sink.setCardNumber(source.getCardNumber().toString());
        sink.setAmount(source.getAmount());
        sink.setCurrency(source.getCurrency().toString());
        sink.setOrderId(source.getOrderId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private PaymentRejectedAvro convert(final PaymentRejected source) {

        final PaymentRejectedAvro sink = new PaymentRejectedAvro();
        sink.setId(source.getId());
        sink.setReason(source.getReason());
        sink.setOrderId(source.getOrderId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private PaymentRejected convert(final PaymentRejectedAvro source) {

        final PaymentRejected sink = new PaymentRejected();
        sink.setId(source.getId().toString());
        sink.setReason(source.getReason().toString());
        sink.setOrderId(source.getOrderId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }
}
