package io.simplematter.microservices.payment.protocol.internal.snapshot;

import io.simplematter.microservices.common.protocol.Snapshot;

public class Payment implements Snapshot<String, String> {

    private String id;
    private String cardHolder;
    private String cardType;
    private String cardNumber;
    private Double amount;
    private String currency;
    private String orderId;
    private String status;
    private String eventId;
    private String eventName;
    private long timestamp;

    public static Payment create(final io.simplematter.microservices.payment.protocol.internal.state.Payment source, final String eventName) {

        final Payment sink = new Payment();
        sink.setId(source.getId().toString());
        sink.setCardHolder(source.getCardHolder().toString());
        sink.setCardType(source.getCardType().toString());
        sink.setCardNumber(source.getCardNumber().toString());
        sink.setAmount(source.getAmount());
        sink.setCurrency(source.getCurrency().toString());
        sink.setOrderId(source.getOrderId().toString());
        sink.setStatus(source.getStatus().toString());
        sink.setEventId(source.getEventId().toString());
        sink.setEventName(eventName);
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public String getCardType() {
        return cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String getEventId() {
        return eventId;
    }

    public String getEventName() {
        return eventName;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
