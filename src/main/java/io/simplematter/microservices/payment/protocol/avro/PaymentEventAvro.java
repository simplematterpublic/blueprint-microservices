/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package io.simplematter.microservices.payment.protocol.avro;

import org.apache.avro.specific.SpecificData;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class PaymentEventAvro extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 2731793808371674236L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"PaymentEventAvro\",\"namespace\":\"io.simplematter.microservices.payment.protocol.avro\",\"fields\":[{\"name\":\"id\",\"type\":\"string\"},{\"name\":\"correlationId\",\"type\":\"string\"},{\"name\":\"manifest\",\"type\":\"string\"},{\"name\":\"timeout\",\"type\":\"long\",\"default\":10000},{\"name\":\"timestamp\",\"type\":\"long\"},{\"name\":\"paymentProcessed\",\"type\":[{\"type\":\"record\",\"name\":\"PaymentProcessedAvro\",\"namespace\":\"io.simplematter.microservices.payment.protocol.avro.event\",\"fields\":[{\"name\":\"id\",\"type\":\"string\"},{\"name\":\"cardHolder\",\"type\":\"string\"},{\"name\":\"cardType\",\"type\":\"string\"},{\"name\":\"cardNumber\",\"type\":\"string\"},{\"name\":\"amount\",\"type\":\"double\"},{\"name\":\"currency\",\"type\":\"string\"},{\"name\":\"orderId\",\"type\":\"string\"},{\"name\":\"timestamp\",\"type\":\"long\"}]},\"null\"]},{\"name\":\"paymentRejected\",\"type\":[{\"type\":\"record\",\"name\":\"PaymentRejectedAvro\",\"namespace\":\"io.simplematter.microservices.payment.protocol.avro.event\",\"fields\":[{\"name\":\"id\",\"type\":\"string\"},{\"name\":\"reason\",\"type\":\"string\"},{\"name\":\"orderId\",\"type\":\"string\"},{\"name\":\"timestamp\",\"type\":\"long\"}]},\"null\"]}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  @Deprecated public java.lang.CharSequence id;
  @Deprecated public java.lang.CharSequence correlationId;
  @Deprecated public java.lang.CharSequence manifest;
  @Deprecated public long timeout;
  @Deprecated public long timestamp;
  @Deprecated public io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro paymentProcessed;
  @Deprecated public io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro paymentRejected;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public PaymentEventAvro() {}

  /**
   * All-args constructor.
   * @param id The new value for id
   * @param correlationId The new value for correlationId
   * @param manifest The new value for manifest
   * @param timeout The new value for timeout
   * @param timestamp The new value for timestamp
   * @param paymentProcessed The new value for paymentProcessed
   * @param paymentRejected The new value for paymentRejected
   */
  public PaymentEventAvro(java.lang.CharSequence id, java.lang.CharSequence correlationId, java.lang.CharSequence manifest, java.lang.Long timeout, java.lang.Long timestamp, io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro paymentProcessed, io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro paymentRejected) {
    this.id = id;
    this.correlationId = correlationId;
    this.manifest = manifest;
    this.timeout = timeout;
    this.timestamp = timestamp;
    this.paymentProcessed = paymentProcessed;
    this.paymentRejected = paymentRejected;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return id;
    case 1: return correlationId;
    case 2: return manifest;
    case 3: return timeout;
    case 4: return timestamp;
    case 5: return paymentProcessed;
    case 6: return paymentRejected;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: id = (java.lang.CharSequence)value$; break;
    case 1: correlationId = (java.lang.CharSequence)value$; break;
    case 2: manifest = (java.lang.CharSequence)value$; break;
    case 3: timeout = (java.lang.Long)value$; break;
    case 4: timestamp = (java.lang.Long)value$; break;
    case 5: paymentProcessed = (io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro)value$; break;
    case 6: paymentRejected = (io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'id' field.
   * @return The value of the 'id' field.
   */
  public java.lang.CharSequence getId() {
    return id;
  }

  /**
   * Sets the value of the 'id' field.
   * @param value the value to set.
   */
  public void setId(java.lang.CharSequence value) {
    this.id = value;
  }

  /**
   * Gets the value of the 'correlationId' field.
   * @return The value of the 'correlationId' field.
   */
  public java.lang.CharSequence getCorrelationId() {
    return correlationId;
  }

  /**
   * Sets the value of the 'correlationId' field.
   * @param value the value to set.
   */
  public void setCorrelationId(java.lang.CharSequence value) {
    this.correlationId = value;
  }

  /**
   * Gets the value of the 'manifest' field.
   * @return The value of the 'manifest' field.
   */
  public java.lang.CharSequence getManifest() {
    return manifest;
  }

  /**
   * Sets the value of the 'manifest' field.
   * @param value the value to set.
   */
  public void setManifest(java.lang.CharSequence value) {
    this.manifest = value;
  }

  /**
   * Gets the value of the 'timeout' field.
   * @return The value of the 'timeout' field.
   */
  public java.lang.Long getTimeout() {
    return timeout;
  }

  /**
   * Sets the value of the 'timeout' field.
   * @param value the value to set.
   */
  public void setTimeout(java.lang.Long value) {
    this.timeout = value;
  }

  /**
   * Gets the value of the 'timestamp' field.
   * @return The value of the 'timestamp' field.
   */
  public java.lang.Long getTimestamp() {
    return timestamp;
  }

  /**
   * Sets the value of the 'timestamp' field.
   * @param value the value to set.
   */
  public void setTimestamp(java.lang.Long value) {
    this.timestamp = value;
  }

  /**
   * Gets the value of the 'paymentProcessed' field.
   * @return The value of the 'paymentProcessed' field.
   */
  public io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro getPaymentProcessed() {
    return paymentProcessed;
  }

  /**
   * Sets the value of the 'paymentProcessed' field.
   * @param value the value to set.
   */
  public void setPaymentProcessed(io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro value) {
    this.paymentProcessed = value;
  }

  /**
   * Gets the value of the 'paymentRejected' field.
   * @return The value of the 'paymentRejected' field.
   */
  public io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro getPaymentRejected() {
    return paymentRejected;
  }

  /**
   * Sets the value of the 'paymentRejected' field.
   * @param value the value to set.
   */
  public void setPaymentRejected(io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro value) {
    this.paymentRejected = value;
  }

  /**
   * Creates a new PaymentEventAvro RecordBuilder.
   * @return A new PaymentEventAvro RecordBuilder
   */
  public static io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder newBuilder() {
    return new io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder();
  }

  /**
   * Creates a new PaymentEventAvro RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new PaymentEventAvro RecordBuilder
   */
  public static io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder newBuilder(io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder other) {
    return new io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder(other);
  }

  /**
   * Creates a new PaymentEventAvro RecordBuilder by copying an existing PaymentEventAvro instance.
   * @param other The existing instance to copy.
   * @return A new PaymentEventAvro RecordBuilder
   */
  public static io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder newBuilder(io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro other) {
    return new io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder(other);
  }

  /**
   * RecordBuilder for PaymentEventAvro instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<PaymentEventAvro>
    implements org.apache.avro.data.RecordBuilder<PaymentEventAvro> {

    private java.lang.CharSequence id;
    private java.lang.CharSequence correlationId;
    private java.lang.CharSequence manifest;
    private long timeout;
    private long timestamp;
    private io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro paymentProcessed;
    private io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro.Builder paymentProcessedBuilder;
    private io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro paymentRejected;
    private io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro.Builder paymentRejectedBuilder;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.id)) {
        this.id = data().deepCopy(fields()[0].schema(), other.id);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.correlationId)) {
        this.correlationId = data().deepCopy(fields()[1].schema(), other.correlationId);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.manifest)) {
        this.manifest = data().deepCopy(fields()[2].schema(), other.manifest);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.timeout)) {
        this.timeout = data().deepCopy(fields()[3].schema(), other.timeout);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.timestamp)) {
        this.timestamp = data().deepCopy(fields()[4].schema(), other.timestamp);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.paymentProcessed)) {
        this.paymentProcessed = data().deepCopy(fields()[5].schema(), other.paymentProcessed);
        fieldSetFlags()[5] = true;
      }
      if (other.hasPaymentProcessedBuilder()) {
        this.paymentProcessedBuilder = io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro.newBuilder(other.getPaymentProcessedBuilder());
      }
      if (isValidValue(fields()[6], other.paymentRejected)) {
        this.paymentRejected = data().deepCopy(fields()[6].schema(), other.paymentRejected);
        fieldSetFlags()[6] = true;
      }
      if (other.hasPaymentRejectedBuilder()) {
        this.paymentRejectedBuilder = io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro.newBuilder(other.getPaymentRejectedBuilder());
      }
    }

    /**
     * Creates a Builder by copying an existing PaymentEventAvro instance
     * @param other The existing instance to copy.
     */
    private Builder(io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.id)) {
        this.id = data().deepCopy(fields()[0].schema(), other.id);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.correlationId)) {
        this.correlationId = data().deepCopy(fields()[1].schema(), other.correlationId);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.manifest)) {
        this.manifest = data().deepCopy(fields()[2].schema(), other.manifest);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.timeout)) {
        this.timeout = data().deepCopy(fields()[3].schema(), other.timeout);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.timestamp)) {
        this.timestamp = data().deepCopy(fields()[4].schema(), other.timestamp);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.paymentProcessed)) {
        this.paymentProcessed = data().deepCopy(fields()[5].schema(), other.paymentProcessed);
        fieldSetFlags()[5] = true;
      }
      this.paymentProcessedBuilder = null;
      if (isValidValue(fields()[6], other.paymentRejected)) {
        this.paymentRejected = data().deepCopy(fields()[6].schema(), other.paymentRejected);
        fieldSetFlags()[6] = true;
      }
      this.paymentRejectedBuilder = null;
    }

    /**
      * Gets the value of the 'id' field.
      * @return The value.
      */
    public java.lang.CharSequence getId() {
      return id;
    }

    /**
      * Sets the value of the 'id' field.
      * @param value The value of 'id'.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder setId(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.id = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'id' field has been set.
      * @return True if the 'id' field has been set, false otherwise.
      */
    public boolean hasId() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'id' field.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder clearId() {
      id = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'correlationId' field.
      * @return The value.
      */
    public java.lang.CharSequence getCorrelationId() {
      return correlationId;
    }

    /**
      * Sets the value of the 'correlationId' field.
      * @param value The value of 'correlationId'.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder setCorrelationId(java.lang.CharSequence value) {
      validate(fields()[1], value);
      this.correlationId = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'correlationId' field has been set.
      * @return True if the 'correlationId' field has been set, false otherwise.
      */
    public boolean hasCorrelationId() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'correlationId' field.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder clearCorrelationId() {
      correlationId = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'manifest' field.
      * @return The value.
      */
    public java.lang.CharSequence getManifest() {
      return manifest;
    }

    /**
      * Sets the value of the 'manifest' field.
      * @param value The value of 'manifest'.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder setManifest(java.lang.CharSequence value) {
      validate(fields()[2], value);
      this.manifest = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'manifest' field has been set.
      * @return True if the 'manifest' field has been set, false otherwise.
      */
    public boolean hasManifest() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'manifest' field.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder clearManifest() {
      manifest = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'timeout' field.
      * @return The value.
      */
    public java.lang.Long getTimeout() {
      return timeout;
    }

    /**
      * Sets the value of the 'timeout' field.
      * @param value The value of 'timeout'.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder setTimeout(long value) {
      validate(fields()[3], value);
      this.timeout = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'timeout' field has been set.
      * @return True if the 'timeout' field has been set, false otherwise.
      */
    public boolean hasTimeout() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'timeout' field.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder clearTimeout() {
      fieldSetFlags()[3] = false;
      return this;
    }

    /**
      * Gets the value of the 'timestamp' field.
      * @return The value.
      */
    public java.lang.Long getTimestamp() {
      return timestamp;
    }

    /**
      * Sets the value of the 'timestamp' field.
      * @param value The value of 'timestamp'.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder setTimestamp(long value) {
      validate(fields()[4], value);
      this.timestamp = value;
      fieldSetFlags()[4] = true;
      return this;
    }

    /**
      * Checks whether the 'timestamp' field has been set.
      * @return True if the 'timestamp' field has been set, false otherwise.
      */
    public boolean hasTimestamp() {
      return fieldSetFlags()[4];
    }


    /**
      * Clears the value of the 'timestamp' field.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder clearTimestamp() {
      fieldSetFlags()[4] = false;
      return this;
    }

    /**
      * Gets the value of the 'paymentProcessed' field.
      * @return The value.
      */
    public io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro getPaymentProcessed() {
      return paymentProcessed;
    }

    /**
      * Sets the value of the 'paymentProcessed' field.
      * @param value The value of 'paymentProcessed'.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder setPaymentProcessed(io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro value) {
      validate(fields()[5], value);
      this.paymentProcessedBuilder = null;
      this.paymentProcessed = value;
      fieldSetFlags()[5] = true;
      return this;
    }

    /**
      * Checks whether the 'paymentProcessed' field has been set.
      * @return True if the 'paymentProcessed' field has been set, false otherwise.
      */
    public boolean hasPaymentProcessed() {
      return fieldSetFlags()[5];
    }

    /**
     * Gets the Builder instance for the 'paymentProcessed' field and creates one if it doesn't exist yet.
     * @return This builder.
     */
    public io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro.Builder getPaymentProcessedBuilder() {
      if (paymentProcessedBuilder == null) {
        if (hasPaymentProcessed()) {
          setPaymentProcessedBuilder(io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro.newBuilder(paymentProcessed));
        } else {
          setPaymentProcessedBuilder(io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro.newBuilder());
        }
      }
      return paymentProcessedBuilder;
    }

    /**
     * Sets the Builder instance for the 'paymentProcessed' field
     * @param value The builder instance that must be set.
     * @return This builder.
     */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder setPaymentProcessedBuilder(io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro.Builder value) {
      clearPaymentProcessed();
      paymentProcessedBuilder = value;
      return this;
    }

    /**
     * Checks whether the 'paymentProcessed' field has an active Builder instance
     * @return True if the 'paymentProcessed' field has an active Builder instance
     */
    public boolean hasPaymentProcessedBuilder() {
      return paymentProcessedBuilder != null;
    }

    /**
      * Clears the value of the 'paymentProcessed' field.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder clearPaymentProcessed() {
      paymentProcessed = null;
      paymentProcessedBuilder = null;
      fieldSetFlags()[5] = false;
      return this;
    }

    /**
      * Gets the value of the 'paymentRejected' field.
      * @return The value.
      */
    public io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro getPaymentRejected() {
      return paymentRejected;
    }

    /**
      * Sets the value of the 'paymentRejected' field.
      * @param value The value of 'paymentRejected'.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder setPaymentRejected(io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro value) {
      validate(fields()[6], value);
      this.paymentRejectedBuilder = null;
      this.paymentRejected = value;
      fieldSetFlags()[6] = true;
      return this;
    }

    /**
      * Checks whether the 'paymentRejected' field has been set.
      * @return True if the 'paymentRejected' field has been set, false otherwise.
      */
    public boolean hasPaymentRejected() {
      return fieldSetFlags()[6];
    }

    /**
     * Gets the Builder instance for the 'paymentRejected' field and creates one if it doesn't exist yet.
     * @return This builder.
     */
    public io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro.Builder getPaymentRejectedBuilder() {
      if (paymentRejectedBuilder == null) {
        if (hasPaymentRejected()) {
          setPaymentRejectedBuilder(io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro.newBuilder(paymentRejected));
        } else {
          setPaymentRejectedBuilder(io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro.newBuilder());
        }
      }
      return paymentRejectedBuilder;
    }

    /**
     * Sets the Builder instance for the 'paymentRejected' field
     * @param value The builder instance that must be set.
     * @return This builder.
     */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder setPaymentRejectedBuilder(io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro.Builder value) {
      clearPaymentRejected();
      paymentRejectedBuilder = value;
      return this;
    }

    /**
     * Checks whether the 'paymentRejected' field has an active Builder instance
     * @return True if the 'paymentRejected' field has an active Builder instance
     */
    public boolean hasPaymentRejectedBuilder() {
      return paymentRejectedBuilder != null;
    }

    /**
      * Clears the value of the 'paymentRejected' field.
      * @return This builder.
      */
    public io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro.Builder clearPaymentRejected() {
      paymentRejected = null;
      paymentRejectedBuilder = null;
      fieldSetFlags()[6] = false;
      return this;
    }

    @Override
    public PaymentEventAvro build() {
      try {
        PaymentEventAvro record = new PaymentEventAvro();
        record.id = fieldSetFlags()[0] ? this.id : (java.lang.CharSequence) defaultValue(fields()[0]);
        record.correlationId = fieldSetFlags()[1] ? this.correlationId : (java.lang.CharSequence) defaultValue(fields()[1]);
        record.manifest = fieldSetFlags()[2] ? this.manifest : (java.lang.CharSequence) defaultValue(fields()[2]);
        record.timeout = fieldSetFlags()[3] ? this.timeout : (java.lang.Long) defaultValue(fields()[3]);
        record.timestamp = fieldSetFlags()[4] ? this.timestamp : (java.lang.Long) defaultValue(fields()[4]);
        if (paymentProcessedBuilder != null) {
          record.paymentProcessed = this.paymentProcessedBuilder.build();
        } else {
          record.paymentProcessed = fieldSetFlags()[5] ? this.paymentProcessed : (io.simplematter.microservices.payment.protocol.avro.event.PaymentProcessedAvro) defaultValue(fields()[5]);
        }
        if (paymentRejectedBuilder != null) {
          record.paymentRejected = this.paymentRejectedBuilder.build();
        } else {
          record.paymentRejected = fieldSetFlags()[6] ? this.paymentRejected : (io.simplematter.microservices.payment.protocol.avro.event.PaymentRejectedAvro) defaultValue(fields()[6]);
        }
        return record;
      } catch (Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  private static final org.apache.avro.io.DatumWriter
    WRITER$ = new org.apache.avro.specific.SpecificDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  private static final org.apache.avro.io.DatumReader
    READER$ = new org.apache.avro.specific.SpecificDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
