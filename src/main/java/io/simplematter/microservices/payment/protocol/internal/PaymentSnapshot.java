package io.simplematter.microservices.payment.protocol.internal;

import io.simplematter.microservices.common.protocol.Envelope;
import io.simplematter.microservices.common.protocol.Snapshot;

public class PaymentSnapshot extends Envelope<Snapshot<String, String>> {

    public PaymentSnapshot(final String id, final String correlationId, final long timeout, final long timestamp) {

        setId(id);
        setCorrelationId(correlationId);
        setTimeout(timeout);
        setTimestamp(timestamp);
    }

    public PaymentSnapshot(final String id, final String correlationId, final long timeout) {

        this(id, correlationId, timeout, System.currentTimeMillis());
    }

    public PaymentSnapshot(final String id, final String correlationId) {

        this(id, correlationId, 10000);
    }

    public PaymentSnapshot() {

    }

}
