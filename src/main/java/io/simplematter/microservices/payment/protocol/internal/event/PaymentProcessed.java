package io.simplematter.microservices.payment.protocol.internal.event;

import io.simplematter.microservices.common.protocol.Event;

public class PaymentProcessed implements Event<String> {

    private String id;
    private String cardHolder;
    private String cardType;
    private String cardNumber;
    private Double amount;
    private String currency;
    private String orderId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public String getCardType() {
        return cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getOrderId() {
        return orderId;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
