package io.simplematter.microservices.payment.protocol.converter;

import io.simplematter.microservices.common.protocol.Converter;
import io.simplematter.microservices.payment.protocol.avro.PaymentSnapshotAvro;
import io.simplematter.microservices.payment.protocol.avro.snapshot.PaymentAvro;
import io.simplematter.microservices.payment.protocol.internal.PaymentSnapshot;
import io.simplematter.microservices.payment.protocol.internal.snapshot.Payment;

public class SnapshotConverter implements Converter<PaymentSnapshot, PaymentSnapshotAvro> {

    @Override
    public PaymentSnapshotAvro serialize(final PaymentSnapshot source) {

        final PaymentSnapshotAvro sink = new PaymentSnapshotAvro();
        sink.setId(source.getId());
        sink.setCorrelationId(source.getCorrelationId());
        sink.setManifest(source.getManifest());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() instanceof Payment) {
            sink.setPayload(serialize((Payment) source.getPayload()));
        }
        return sink;
    }

    @Override
    public PaymentSnapshot deserialize(final PaymentSnapshotAvro source) {

        final PaymentSnapshot sink = new PaymentSnapshot();
        sink.setId(source.getId().toString());
        sink.setCorrelationId(source.getCorrelationId().toString());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() != null) {
            sink.setPayload(deserialize(source.getPayload()));
        }
        return sink;
    }

    private PaymentAvro serialize(final Payment source) {

        final PaymentAvro sink = new PaymentAvro();
        sink.setId(source.getId());
        sink.setCardHolder(source.getCardHolder());
        sink.setCardType(source.getCardType());
        sink.setCardNumber(source.getCardNumber());
        sink.setAmount(source.getAmount());
        sink.setCurrency(source.getCurrency());
        sink.setOrderId(source.getOrderId());
        sink.setStatus(source.getStatus());
        sink.setEventId(source.getEventId());
        sink.setEventName(source.getEventName());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private Payment deserialize(final PaymentAvro source) {

        final Payment sink = new Payment();
        sink.setId(source.getId().toString());
        sink.setCardHolder(source.getCardHolder().toString());
        sink.setCardType(source.getCardType().toString());
        sink.setCardNumber(source.getCardNumber().toString());
        sink.setAmount(source.getAmount());
        sink.setCurrency(source.getCurrency().toString());
        sink.setOrderId(source.getOrderId().toString());
        sink.setStatus(source.getStatus().toString());
        sink.setEventId(source.getEventId().toString());
        sink.setEventName(source.getEventName().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }
}
