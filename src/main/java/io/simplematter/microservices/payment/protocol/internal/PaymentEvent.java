package io.simplematter.microservices.payment.protocol.internal;

import io.simplematter.microservices.common.protocol.Envelope;
import io.simplematter.microservices.common.protocol.Event;

public class PaymentEvent extends Envelope<Event<String>> {

    public PaymentEvent(final String id, final String correlationId, final long timeout, final long timestamp) {

        setId(id);
        setCorrelationId(correlationId);
        setTimeout(timeout);
        setTimestamp(timestamp);
    }

    public PaymentEvent(final String id, final String correlationId, final long timeout) {

        this(id, correlationId, timeout, System.currentTimeMillis());
    }

    public PaymentEvent(final String id, final String correlationId) {

        this(id, correlationId, 1000);
    }

    public PaymentEvent() {

    }

}
