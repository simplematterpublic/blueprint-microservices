package io.simplematter.microservices.payment.processor.projector;

import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.projection.Store;
import io.simplematter.microservices.payment.StaticConfigs;
import io.simplematter.microservices.payment.protocol.internal.PaymentEvent;
import io.simplematter.microservices.payment.protocol.internal.PaymentSnapshot;
import io.simplematter.microservices.payment.protocol.internal.snapshot.Payment;
import io.simplematter.microservices.payment.protocol.serdes.EventSerdes;
import io.simplematter.microservices.payment.protocol.serdes.SnapshotSerdes;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;

import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class PaymentProjector {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Properties config;
    private final Store<String, Payment> store;
    private KafkaStreams streams;

    public PaymentProjector(final Properties config, final Store<String, Payment> store) {

        Objects.requireNonNull(config.getProperty("application.id"));
        Objects.requireNonNull(config.getProperty("schema.registry.url"));
        this.config = config;
        this.store = store;
    }

    public void start() {

        logger.info("Starting " + getClass().getSimpleName());

        final String applicationId = config.getProperty("application.id");
        final String registryUrl = config.getProperty("schema.registry.url");

        logger.info("Registering applicationId: " + applicationId);

        final SchemaRegistryClient schemaRegistry = new CachedSchemaRegistryClient(registryUrl, 100);

        final Serde<PaymentEvent> eventSerde = new EventSerdes(applicationId, schemaRegistry, registryUrl).getSerdes();
        final Serde<PaymentSnapshot> snapshotSerde = new SnapshotSerdes(applicationId, schemaRegistry, registryUrl).getSerdes();

        final KStreamBuilder builder = new KStreamBuilder();
        // builder.newName(CheckoutProjector.class.getSimpleName() + "_" + INDEX);

        final KStream<String, PaymentEvent> events = builder.stream(new Serdes.StringSerde(),
                eventSerde, StaticConfigs.EVENT_TOPIC);

        final KStream<String, PaymentSnapshot> snapshots = builder.stream(new Serdes.StringSerde(),
                snapshotSerde, StaticConfigs.SNAPSHOT_TOPIC);

        final KStream<String, PaymentSnapshot> results = events.join(snapshots, (event, snapshot) -> {
                    PaymentSnapshot result = null;
                    if (event.getPayload().getId().equals(snapshot.getPayload().getEventId())) {
                        result = snapshot;
                    }
                    return result;
                }, JoinWindows.of(TimeUnit.SECONDS.toMillis(10)),
                new Serdes.StringSerde(), eventSerde, snapshotSerde).filter((k, v) -> v != null);


        results.foreach((k, v) -> {
            store.persist((Payment) v.getPayload());
        });

        streams = new KafkaStreams(builder, config);

        streams.start();

        logger.info(getClass().getSimpleName() + " Started");
    }

    public void stop() {

        try {
            logger.info("Stopping " + getClass().getSimpleName());
            streams.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            logger.info(getClass().getSimpleName() + " Stopped");
        }
    }
}
