package io.simplematter.microservices.payment.processor.projector;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.model.MapObject;
import com.rethinkdb.net.Connection;
import io.simplematter.microservices.common.projection.AbstractStore;
import io.simplematter.microservices.common.util.TypeUtil;
import io.simplematter.microservices.payment.protocol.internal.snapshot.Payment;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.HashMap;

public class ProjectorStore extends AbstractStore<String, Payment> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final RethinkDB r = RethinkDB.r;

    public ProjectorStore(final Connection connection, final String table) {

        super(connection, table);
    }

    @Override
    protected Payment convert(final HashMap source) {

        final Payment sink = new Payment();
        sink.setId((String) source.get("id"));
        sink.setCardHolder((String) source.get("cardHolder"));
        sink.setCardType((String) source.get("cardType"));
        sink.setCardNumber((String) source.get("cardNumber"));
        sink.setAmount(TypeUtil.toDouble(source.get("amount")));
        sink.setCurrency((String) source.get("currency"));
        sink.setOrderId((String) source.get("orderId"));
        sink.setStatus((String) source.get("status"));
        sink.setEventId((String) source.get("eventId"));
        sink.setEventName((String) source.get("eventName"));
        sink.setTimestamp((Long) source.get("timestamp"));
        return sink;
    }

    @Override
    protected MapObject convert(final Payment source) {

        final MapObject sink = r.hashMap("id", source.getId())
                .with("cardHolder", source.getCardHolder())
                .with("cardType", source.getCardType())
                .with("cardNumber", source.getCardNumber())
                .with("amount", source.getAmount())
                .with("currency", source.getCurrency())
                .with("orderId", source.getOrderId())
                .with("status", source.getStatus())
                .with("eventId", source.getEventId())
                .with("eventName", source.getEventName())
                .with("timestamp", source.getTimestamp());
        return sink;
    }
}
