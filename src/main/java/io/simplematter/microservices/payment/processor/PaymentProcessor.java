package io.simplematter.microservices.payment.processor;

import io.simplematter.microservices.common.protocol.Command;
import io.simplematter.microservices.payment.protocol.internal.PaymentCommand;
import io.simplematter.microservices.payment.protocol.internal.PaymentEvent;
import io.simplematter.microservices.payment.protocol.internal.PaymentState;
import io.simplematter.microservices.payment.protocol.internal.command.ProcessPayment;
import io.simplematter.microservices.payment.protocol.internal.event.PaymentProcessed;
import io.simplematter.microservices.payment.protocol.internal.event.PaymentRejected;
import io.simplematter.microservices.payment.protocol.internal.state.Payment;
import io.simplematter.microservices.common.kafka.streams.processor.StoreProcessor;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorSupplier;

public class PaymentProcessor implements ProcessorSupplier<String, PaymentCommand> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String storeName;
    private final String eventDestination;
    private final String errorDestination;

    public PaymentProcessor(final String storeName, final String eventDestination, final String errorDestination) {

        this.storeName = storeName;
        this.eventDestination = eventDestination;
        this.errorDestination = errorDestination;
    }

    public Processor<String, PaymentCommand> get() {

        return new StoreProcessor<String, PaymentCommand, PaymentState>() {

            public String getStoreName() {

                return storeName;
            }

            public String getEventDestination() {

                return eventDestination;
            }

            public String getErrorDestination() {

                return errorDestination;
            }

            @Override
            public void process(final String key, final PaymentCommand commandEnvelope) {

                try {
                    doProcess(key, commandEnvelope);
                } catch (Exception e) {
                    logger.error(e);
                    // TODO: Handle Failure
                } finally {
                    context().commit();
                }
            }

            private void doProcess(final String key, final PaymentCommand commandEnvelope) {

                final PaymentState stateEnvelope = new PaymentState(commandEnvelope.getId(), commandEnvelope.getCorrelationId());
                final PaymentEvent eventEnvelope = new PaymentEvent(commandEnvelope.getId(), commandEnvelope.getCorrelationId());
                final Command command = commandEnvelope.getPayload();
                logger.info("ServiceProcessor received message: " + command.getClass().getName());
                if (command instanceof ProcessPayment) {
                    final Payment currentState = getState(((ProcessPayment) command).getId());
                    if (currentState == null || currentState.getStatus().equals(Payment.PROCESSED)) {
                        if (((ProcessPayment) command).getAmount() > 5000) {
                            final PaymentRejected event = reject((ProcessPayment) command, "Not enough liquidity on the wallet");
                            eventEnvelope.setPayload(event);
                            final Payment state = Payment.create(accept((ProcessPayment) command), context().offset()).update(event, context().offset());
                            stateEnvelope.setPayload(state);
                            getStore().put(state.getId(), stateEnvelope);
                            context().forward(state.getId(), eventEnvelope, getEventDestination());
                        } else {
                            final PaymentProcessed event = accept((ProcessPayment) command);
                            eventEnvelope.setPayload(event);
                            final Payment state = Payment.create(event, context().offset());
                            stateEnvelope.setPayload(state);
                            getStore().put(state.getId(), stateEnvelope);
                            context().forward(state.getId(), eventEnvelope, getEventDestination());
                        }
                    } else {
                        throw new RuntimeException("Status not found");
                        // TODO: Emit business reject message
                    }
                } else {
                    throw new RuntimeException("Received unknown message: " + command.getClass().getName());
                }
            }

            /**
             *
             * @param command The payment Command
             * @return The Payment Event
             */
            private PaymentProcessed accept(final ProcessPayment command) {

                final PaymentProcessed event = new PaymentProcessed();
                event.setId(command.getId());
                event.setCardHolder(command.getCardHolder());
                event.setCardType(command.getCardType());
                event.setCardNumber(command.getCardNumber());
                event.setAmount(command.getAmount());
                event.setCurrency(command.getCurrency());
                event.setOrderId(command.getOrderId());
                event.setTimestamp(System.currentTimeMillis());
                return event;
            }

            /**
             *
             * @param command The Payment Command
             * @param reason The Rejection Reason
             * @return The Payment Event
             */
            private PaymentRejected reject(final ProcessPayment command, final String reason) {

                final PaymentRejected event = new PaymentRejected();
                event.setId(command.getId());
                event.setReason(reason);
                event.setOrderId(command.getOrderId());
                event.setTimestamp(System.currentTimeMillis());
                return event;
            }
        };
    }
}
