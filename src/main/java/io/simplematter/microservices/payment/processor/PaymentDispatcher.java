package io.simplematter.microservices.payment.processor;

import io.simplematter.microservices.payment.protocol.internal.PaymentEvent;
import io.simplematter.microservices.payment.protocol.internal.PaymentSnapshot;
import io.simplematter.microservices.payment.protocol.internal.PaymentState;
import io.simplematter.microservices.common.kafka.streams.processor.StoreProcessor;
import io.simplematter.microservices.payment.protocol.internal.snapshot.Payment;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorSupplier;

public class PaymentDispatcher implements ProcessorSupplier<String, PaymentEvent> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String storeName;
    private final String eventDestination;
    private final String snapshotDestination;

    public PaymentDispatcher(final String storeName, final String eventDestination, final String snapshotDestination) {

        this.storeName = storeName;
        this.eventDestination = eventDestination;
        this.snapshotDestination = snapshotDestination;
    }

    public Processor<String, PaymentEvent> get() {

        return new StoreProcessor<String, PaymentEvent, PaymentState>()  {

            public String getStoreName() {

                return storeName;
            }

            public String getEventDestination() {

                return eventDestination;
            }

            public String getSnapshotDestination() {

                return snapshotDestination;
            }

            @Override
            public void process(final String key, final PaymentEvent eventEnvelope) {

                logger.info("DispatcherProcessor received message: " + eventEnvelope.getPayload().getClass().getName());
                //System.out.println(getClass().getSimpleName() + " <- (" + key + ", " + eventEnvelope + ")");
                //System.out.println(getClass().getSimpleName() + " -> (" + key + ", " + eventEnvelope + "->SNAPSHOT)");
                final PaymentSnapshot snapshotEnvelope = new PaymentSnapshot(eventEnvelope.getId(), eventEnvelope.getCorrelationId());
                snapshotEnvelope.setPayload(Payment.create(getState(key), eventEnvelope.getClass().getSimpleName()));
                context().forward(eventEnvelope.getId(), snapshotEnvelope, getSnapshotDestination());
                context().forward(eventEnvelope.getId(), eventEnvelope, getEventDestination());
                context().commit();
            }

        };
    }

}
