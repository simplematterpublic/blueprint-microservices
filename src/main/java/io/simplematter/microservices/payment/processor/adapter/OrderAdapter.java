package io.simplematter.microservices.payment.processor.adapter;

import io.simplematter.microservices.common.protocol.Command;
import io.simplematter.microservices.common.protocol.Event;
import io.simplematter.microservices.order.protocol.internal.OrderEvent;
import io.simplematter.microservices.order.protocol.internal.event.OrderValidated;
import io.simplematter.microservices.payment.protocol.internal.PaymentCommand;
import io.simplematter.microservices.payment.protocol.internal.command.ProcessPayment;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.streams.processor.AbstractProcessor;

import java.rmi.server.UID;

public class OrderAdapter extends AbstractProcessor<String, OrderEvent> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void process(final String key, final OrderEvent orderEnvelope) {

        try {
            doProcess(key, orderEnvelope);
        } catch (Exception e) {
            logger.error(e);
        } finally {
            context().commit();
        }
    }

    private void doProcess(final String key, final OrderEvent orderEnvelope) {

        final PaymentCommand paymentCommand = new PaymentCommand(orderEnvelope.getId(), orderEnvelope.getCorrelationId());
        final Event event = orderEnvelope.getPayload();
        logger.info("AdapterProcessor received message: " + orderEnvelope.getPayload().getClass().getName());
        if (event instanceof OrderValidated) {
            paymentCommand.setPayload(convert((OrderValidated) event));
            context().forward(key, paymentCommand);
        } else {
            if (logger.isDebugEnabled()) {
                logger.warn("AdapterProcessor skipping unhandled message: " + orderEnvelope.getPayload().getClass().getName());
            }
        }
    }

    private Command convert(final OrderValidated event) {

        final ProcessPayment command = new ProcessPayment();
        command.setId(new UID().toString());
        command.setCardHolder(event.getPaymentDetail().getCardHolder());
        command.setCardType(event.getPaymentDetail().getCardType());
        command.setCardNumber(event.getPaymentDetail().getCardNumber());
        command.setAmount(event.getPaymentDetail().getAmount());
        command.setCurrency(event.getPaymentDetail().getCurrency());
        command.setOrderId(event.getId());
        command.setTimestamp(event.getTimestamp());
        return command;
    }
}
