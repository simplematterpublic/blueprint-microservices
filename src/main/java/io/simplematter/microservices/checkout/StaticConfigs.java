package io.simplematter.microservices.checkout;

public class StaticConfigs {

    public static final int INDEX = 1;

    public static final String DOMAIN = "CHECKOUT";
    public static final String APPLICATION_ID = DOMAIN + "_" + INDEX;

    public static final String COMMAND_PROCESSOR = "COMMAND_PROCESSOR";
    public static final String ROUTING_PROCESSOR = "ROUTING_PROCESSOR";

    public static final String COMMAND_SOURCE = "COMMAND_SOURCE";

    public static final String COMMAND_SINK = "COMMAND_SINK";
    public static final String ERROR_SINK = "ERROR_SINK";
    public static final String SNAPSHOT_SINK = "SNAPSHOT_SINK";
    public static final String EVENT_SINK = "EVENT_SINK";

    public static final String STATE_STORE = "checkout_store_" + INDEX;
    public static final String COMMAND_TOPIC = "checkout_commands_" + INDEX;
    public static final String SNAPSHOT_TOPIC = "checkout_snapshots_" + INDEX;
    public static final String EVENT_TOPIC = "checkout_events_" + INDEX;
    public static final String ERROR_TOPIC = "checkout_errors_" + INDEX;

}
