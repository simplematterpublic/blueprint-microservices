package io.simplematter.microservices.checkout.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.checkout.protocol.avro.CheckoutCommandAvro;
import io.simplematter.microservices.checkout.protocol.converter.CommandConverter;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutCommand;
import io.simplematter.microservices.common.protocol.serdes.SerdesSupplier;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;

public class CommandSerdes extends SerdesSupplier<CheckoutCommand> {

    public CommandSerdes(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        super(applicationId, schemaRegistry, registryUrl);
    }

    @Override
    public AvroSerializer<CheckoutCommand> getSerializer() {

        final AvroSerializer<CheckoutCommand> serializer = new AvroSerializer(getApplicationId(), getSchemaRegistry(), new CommandConverter());

        serializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return serializer;
    }

    @Override
    public AvroDeserializer<CheckoutCommand> getDeserializer() {

        final AvroDeserializer<CheckoutCommand> deserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(), new CommandConverter(), CheckoutCommandAvro.SCHEMA$);

        deserializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return deserializer;
    }

    @Override
    public Serde<CheckoutCommand> getSerdes() {

        return Serdes.serdeFrom(getSerializer(), getDeserializer());
    }

}
