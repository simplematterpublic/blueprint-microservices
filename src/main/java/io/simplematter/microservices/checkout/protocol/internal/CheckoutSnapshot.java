package io.simplematter.microservices.checkout.protocol.internal;

import io.simplematter.microservices.common.protocol.Envelope;
import io.simplematter.microservices.common.protocol.Snapshot;

public class CheckoutSnapshot extends Envelope<Snapshot<String, String>> {

    public CheckoutSnapshot(final String id, final String correlationId, final long timeout, final long timestamp) {

        setId(id);
        setCorrelationId(correlationId);
        setTimeout(timeout);
        setTimestamp(timestamp);
    }

    public CheckoutSnapshot(final String id, final String correlationId, final long timeout) {

        this(id, correlationId, timeout, System.currentTimeMillis());
    }

    public CheckoutSnapshot(final String id, final String correlationId) {

        this(id, correlationId, 10000);
    }

    public CheckoutSnapshot() {

    }

}
