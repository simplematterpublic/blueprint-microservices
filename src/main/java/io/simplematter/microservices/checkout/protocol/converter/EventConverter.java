package io.simplematter.microservices.checkout.protocol.converter;

import io.simplematter.microservices.checkout.protocol.avro.CheckoutEventAvro;
import io.simplematter.microservices.checkout.protocol.avro.event.CheckoutAcknowledgedAvro;
import io.simplematter.microservices.checkout.protocol.avro.event.CheckoutConfirmedAvro;
import io.simplematter.microservices.checkout.protocol.avro.event.CheckoutRejectedAvro;
import io.simplematter.microservices.checkout.protocol.avro.event.CheckoutRequestedAvro;
import io.simplematter.microservices.checkout.protocol.avro.model.PaymentDetailAvro;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutEvent;
import io.simplematter.microservices.checkout.protocol.internal.event.CheckoutAcknowledged;
import io.simplematter.microservices.checkout.protocol.internal.event.CheckoutConfirmed;
import io.simplematter.microservices.checkout.protocol.internal.event.CheckoutRejected;
import io.simplematter.microservices.checkout.protocol.internal.event.CheckoutRequested;
import io.simplematter.microservices.checkout.protocol.internal.model.PaymentDetail;
import io.simplematter.microservices.common.protocol.Converter;

public class EventConverter implements Converter<CheckoutEvent, CheckoutEventAvro> {

    @Override
    public CheckoutEventAvro serialize(final CheckoutEvent source) {

        final CheckoutEventAvro sink = new CheckoutEventAvro();
        sink.setId(source.getId());
        sink.setCorrelationId(source.getCorrelationId());
        sink.setTimeout(source.getTimeout());
        sink.setManifest(source.getManifest());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() instanceof CheckoutRequested) {
            sink.setCheckoutRequested(convert((CheckoutRequested)
                    source.getPayload()));
        } else if (source.getPayload() instanceof CheckoutAcknowledged) {
            sink.setCheckoutAcknowledged(convert((CheckoutAcknowledged)
                    source.getPayload()));
        } else if (source.getPayload() instanceof CheckoutConfirmed) {
            sink.setCheckoutConfirmed(convert((CheckoutConfirmed)
                    source.getPayload()));
        } else if (source.getPayload() instanceof CheckoutRejected) {
            sink.setCheckoutRejected(convert((CheckoutRejected)
                    source.getPayload()));
        }
        return sink;
    }

    @Override
    public CheckoutEvent deserialize(final CheckoutEventAvro source) {

        final CheckoutEvent sink = new CheckoutEvent();
        sink.setId(source.getId().toString());
        sink.setCorrelationId(source.getCorrelationId().toString());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getCheckoutRequested() != null) {
            sink.setPayload(convert(source.getCheckoutRequested()));
        } else if (source.getCheckoutAcknowledged() != null) {
            sink.setPayload(convert(source.getCheckoutAcknowledged()));
        } else if (source.getCheckoutConfirmed() != null) {
            sink.setPayload(convert(source.getCheckoutConfirmed()));
        } else if (source.getCheckoutRejected() != null) {
            sink.setPayload(convert(source.getCheckoutRejected()));
        }
        return sink;
    }

    private CheckoutRequestedAvro convert(final CheckoutRequested source) {

        final PaymentDetailAvro paymentDetail = new PaymentDetailAvro();
        paymentDetail.setCardHolder(source.getPaymentDetail().getCardHolder());
        paymentDetail.setCardType(source.getPaymentDetail().getCardType());
        paymentDetail.setCardNumber(source.getPaymentDetail().getCardNumber());
        paymentDetail.setAmount(source.getPaymentDetail().getAmount());
        paymentDetail.setCurrency(source.getPaymentDetail().getCurrency());

        final CheckoutRequestedAvro sink = new CheckoutRequestedAvro();
        sink.setId(source.getId());
        sink.setSku(source.getSku());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setPaymentDetail(paymentDetail);
        sink.setCustomerId(source.getCustomerId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private CheckoutRequested convert(CheckoutRequestedAvro source) {

        final PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setCardHolder(source.getPaymentDetail().getCardHolder().toString());
        paymentDetail.setCardType(source.getPaymentDetail().getCardType().toString());
        paymentDetail.setCardNumber(source.getPaymentDetail().getCardNumber().toString());
        paymentDetail.setAmount(source.getPaymentDetail().getAmount());
        paymentDetail.setCurrency(source.getPaymentDetail().getCurrency().toString());

        final CheckoutRequested sink = new CheckoutRequested();
        sink.setId(source.getId().toString());
        sink.setSku(source.getSku().toString());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setPaymentDetail(paymentDetail);
        sink.setCustomerId(source.getCustomerId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private CheckoutAcknowledgedAvro convert(final CheckoutAcknowledged source) {

        final CheckoutAcknowledgedAvro sink = new CheckoutAcknowledgedAvro();
        sink.setId(source.getId());
        sink.setOrderId(source.getOrderId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private CheckoutAcknowledged convert(final CheckoutAcknowledgedAvro source) {

        final CheckoutAcknowledged sink = new CheckoutAcknowledged();
        sink.setId(source.getId().toString());
        sink.setOrderId(source.getOrderId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private CheckoutConfirmedAvro convert(final CheckoutConfirmed source) {

        final CheckoutConfirmedAvro sink = new CheckoutConfirmedAvro();
        sink.setId(source.getId());
        sink.setOrderId(source.getOrderId());
        sink.setPaymentId(source.getPaymentId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private CheckoutConfirmed convert(final CheckoutConfirmedAvro source) {

        final CheckoutConfirmed sink = new CheckoutConfirmed();
        sink.setId(source.getId().toString());
        sink.setOrderId(source.getOrderId().toString());
        sink.setPaymentId(source.getPaymentId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private CheckoutRejectedAvro convert(CheckoutRejected source) {

        final CheckoutRejectedAvro sink = new CheckoutRejectedAvro();
        sink.setId(source.getId());
        sink.setReason(source.getReason());
        sink.setOrderId(source.getOrderId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private CheckoutRejected convert(final CheckoutRejectedAvro source) {

        final CheckoutRejected sink = new CheckoutRejected();
        sink.setId(source.getId().toString());
        sink.setReason(source.getReason().toString());
        sink.setOrderId(source.getOrderId() == null ? null : source.getOrderId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }
}
