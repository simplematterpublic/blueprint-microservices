package io.simplematter.microservices.checkout.protocol.internal.command;

import io.simplematter.microservices.common.protocol.Command;

public class ConfirmCheckout implements Command<String> {

    private String id;
    private String orderId;
    private String paymentId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
