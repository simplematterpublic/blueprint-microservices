package io.simplematter.microservices.checkout.protocol.converter;

import io.simplematter.microservices.checkout.protocol.avro.CheckoutCommandAvro;
import io.simplematter.microservices.checkout.protocol.avro.command.AcknowledgeCheckoutAvro;
import io.simplematter.microservices.checkout.protocol.avro.command.ConfirmCheckoutAvro;
import io.simplematter.microservices.checkout.protocol.avro.command.RejectCheckoutAvro;
import io.simplematter.microservices.checkout.protocol.avro.command.RequestCheckoutAvro;
import io.simplematter.microservices.checkout.protocol.avro.model.PaymentDetailAvro;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutCommand;
import io.simplematter.microservices.checkout.protocol.internal.command.AcknowledgeCheckout;
import io.simplematter.microservices.checkout.protocol.internal.command.ConfirmCheckout;
import io.simplematter.microservices.checkout.protocol.internal.command.RejectCheckout;
import io.simplematter.microservices.checkout.protocol.internal.command.RequestCheckout;
import io.simplematter.microservices.checkout.protocol.internal.model.PaymentDetail;
import io.simplematter.microservices.common.protocol.Converter;

public class CommandConverter implements Converter<CheckoutCommand, CheckoutCommandAvro> {

    @Override
    public CheckoutCommandAvro serialize(final CheckoutCommand source) {

        final CheckoutCommandAvro sink = new CheckoutCommandAvro();
        sink.setId(source.getId());
        sink.setCorrelationId(source.getCorrelationId());
        sink.setTimeout(source.getTimeout());
        sink.setManifest(source.getManifest());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() instanceof RequestCheckout) {
            sink.setRequestCheckout(convert((RequestCheckout) source.getPayload()));
        } else if (source.getPayload() instanceof AcknowledgeCheckout) {
            sink.setAcknowledgeCheckout(convert((AcknowledgeCheckout) source.getPayload()));
        } else if (source.getPayload() instanceof ConfirmCheckout) {
            sink.setConfirmCheckout(convert((ConfirmCheckout) source.getPayload()));
        } else if (source.getPayload() instanceof RejectCheckout) {
            sink.setRejectCheckout(convert((RejectCheckout) source.getPayload()));
        }
        return sink;
    }

    @Override
    public CheckoutCommand deserialize(final CheckoutCommandAvro source) {

        final CheckoutCommand sink = new CheckoutCommand();
        sink.setId(source.getId().toString());
        sink.setCorrelationId(source.getCorrelationId().toString());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getRequestCheckout() != null) {
            sink.setPayload(convert(source.getRequestCheckout()));
        } else if (source.getAcknowledgeCheckout() != null) {
            sink.setPayload(convert(source.getAcknowledgeCheckout()));
        } else if (source.getConfirmCheckout() != null) {
            sink.setPayload(convert(source.getConfirmCheckout()));
        } else if (source.getRejectCheckout() != null) {
            sink.setPayload(convert(source.getRejectCheckout()));
        }
        return sink;
    }

    private RequestCheckoutAvro convert(final RequestCheckout source) {

        final PaymentDetailAvro paymentDetail = new PaymentDetailAvro();
        paymentDetail.setCardHolder(source.getPaymentDetail().getCardHolder());
        paymentDetail.setCardType(source.getPaymentDetail().getCardType());
        paymentDetail.setCardNumber(source.getPaymentDetail().getCardNumber());
        paymentDetail.setAmount(source.getPaymentDetail().getAmount());
        paymentDetail.setCurrency(source.getPaymentDetail().getCurrency());

        final RequestCheckoutAvro sink = new RequestCheckoutAvro();
        sink.setId(source.getId());
        sink.setSku(source.getSku());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setPaymentDetail(paymentDetail);
        sink.setCustomerId(source.getCustomerId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private RequestCheckout convert(final RequestCheckoutAvro source) {

        final PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setCardHolder(source.getPaymentDetail().getCardHolder().toString());
        paymentDetail.setCardType(source.getPaymentDetail().getCardType().toString());
        paymentDetail.setCardNumber(source.getPaymentDetail().getCardNumber().toString());
        paymentDetail.setAmount(source.getPaymentDetail().getAmount());
        paymentDetail.setCurrency(source.getPaymentDetail().getCurrency().toString());

        final RequestCheckout sink = new RequestCheckout();
        sink.setId(source.getId().toString());
        sink.setSku(source.getSku().toString());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setPaymentDetail(paymentDetail);
        sink.setCustomerId(source.getCustomerId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private AcknowledgeCheckoutAvro convert(AcknowledgeCheckout source) {

        final AcknowledgeCheckoutAvro sink = new AcknowledgeCheckoutAvro();
        sink.setId(source.getId());
        sink.setOrderId(source.getOrderId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private AcknowledgeCheckout convert(final AcknowledgeCheckoutAvro source) {

        final AcknowledgeCheckout sink = new AcknowledgeCheckout();
        sink.setId(source.getId().toString());
        sink.setOrderId(source.getOrderId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private ConfirmCheckoutAvro convert(ConfirmCheckout source) {

        final ConfirmCheckoutAvro sink = new ConfirmCheckoutAvro();
        sink.setId(source.getId());
        sink.setOrderId(source.getOrderId());
        sink.setPaymentId(source.getPaymentId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private ConfirmCheckout convert(final ConfirmCheckoutAvro source) {

        final ConfirmCheckout sink = new ConfirmCheckout();
        sink.setId(source.getId().toString());
        sink.setOrderId(source.getOrderId().toString());
        sink.setPaymentId(source.getPaymentId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private RejectCheckoutAvro convert(final RejectCheckout source) {

        final RejectCheckoutAvro sink = new RejectCheckoutAvro();
        sink.setId(source.getId());
        sink.setReason(source.getReason());
        sink.setOrderId(source.getOrderId());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private RejectCheckout convert(final RejectCheckoutAvro source) {

        final RejectCheckout sink = new RejectCheckout();
        sink.setId(source.getId().toString());
        sink.setReason(source.getReason().toString());
        sink.setOrderId(source.getOrderId() == null ? null : source.getOrderId().toString());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }
}
