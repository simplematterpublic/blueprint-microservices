package io.simplematter.microservices.checkout.protocol.internal;

import io.simplematter.microservices.common.protocol.Command;
import io.simplematter.microservices.common.protocol.Envelope;

public class CheckoutCommand extends Envelope<Command<String>> {

    public CheckoutCommand(final String id, final String correlationId, final long timeout, final long timestamp) {

        setId(id);
        setCorrelationId(correlationId);
        setTimeout(timeout);
        setTimestamp(timestamp);
    }

    public CheckoutCommand(final String id, final String correlationId, final long timeout) {

        this(id, correlationId, timeout, System.currentTimeMillis());
    }

    public CheckoutCommand(final String id, final String correlationId) {

        this(id, correlationId, 10000);
    }

    public CheckoutCommand() {

    }

}
