package io.simplematter.microservices.checkout.protocol.internal.command;

import io.simplematter.microservices.common.protocol.Command;

public class BusinessReject implements Command<String> {

    private String id;
    private String origId;
    private String reason;
    private long timestamp;

    public BusinessReject() {

    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOrigId(String origId) {
        this.origId = origId;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getOrigId() {
        return origId;
    }

    public String getReason() {
        return reason;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
