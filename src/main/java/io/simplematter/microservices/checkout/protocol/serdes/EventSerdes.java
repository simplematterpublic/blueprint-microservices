package io.simplematter.microservices.checkout.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.checkout.protocol.avro.CheckoutEventAvro;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutEvent;
import io.simplematter.microservices.checkout.protocol.converter.EventConverter;
import io.simplematter.microservices.common.protocol.serdes.SerdesSupplier;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;

public class EventSerdes extends SerdesSupplier<CheckoutEvent> {

    public EventSerdes(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        super(applicationId, schemaRegistry, registryUrl);
    }

    @Override
    public AvroSerializer<CheckoutEvent> getSerializer() {

        final AvroSerializer<CheckoutEvent> serializer = new AvroSerializer(getApplicationId(), getSchemaRegistry(), new EventConverter());

        serializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return serializer;
    }

    @Override
    public AvroDeserializer<CheckoutEvent> getDeserializer() {

        final AvroDeserializer<CheckoutEvent> deserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(), new EventConverter(), CheckoutEventAvro.SCHEMA$);

        deserializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return deserializer;
    }

    @Override
    public Serde<CheckoutEvent> getSerdes() {

        return Serdes.serdeFrom(getSerializer(), getDeserializer());
    }

}
