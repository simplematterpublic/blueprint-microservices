package io.simplematter.microservices.checkout.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.checkout.protocol.avro.CheckoutSnapshotAvro;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutSnapshot;
import io.simplematter.microservices.checkout.protocol.converter.SnapshotConverter;
import io.simplematter.microservices.common.protocol.serdes.SerdesSupplier;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;

public class SnapshotSerdes extends SerdesSupplier<CheckoutSnapshot> {

    public SnapshotSerdes(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        super(applicationId, schemaRegistry, registryUrl);
    }

    @Override
    public AvroSerializer<CheckoutSnapshot> getSerializer() {

        final AvroSerializer<CheckoutSnapshot> serializer = new AvroSerializer(getApplicationId(), getSchemaRegistry(), new SnapshotConverter());

        serializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return serializer;
    }

    @Override
    public AvroDeserializer<CheckoutSnapshot> getDeserializer() {

        final AvroDeserializer<CheckoutSnapshot> deserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(), new SnapshotConverter(), CheckoutSnapshotAvro.SCHEMA$);

        deserializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return deserializer;
    }

    @Override
    public Serde<CheckoutSnapshot> getSerdes() {

        return Serdes.serdeFrom(getSerializer(), getDeserializer());
    }

}
