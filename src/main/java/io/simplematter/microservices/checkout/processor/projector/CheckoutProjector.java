package io.simplematter.microservices.checkout.processor.projector;

import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutEvent;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutSnapshot;
import io.simplematter.microservices.checkout.protocol.internal.snapshot.Checkout;
import io.simplematter.microservices.checkout.protocol.serdes.EventSerdes;
import io.simplematter.microservices.checkout.protocol.serdes.SnapshotSerdes;
import io.simplematter.microservices.checkout.StaticConfigs;
import io.simplematter.microservices.common.projection.Store;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;

import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class CheckoutProjector {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Properties config;
    private final Store<String, Checkout> store;
    private KafkaStreams streams;

    public CheckoutProjector(final Properties config, final Store<String, Checkout> store) {

        Objects.requireNonNull(config.getProperty("application.id"));
        Objects.requireNonNull(config.getProperty("schema.registry.url"));
        this.config = config;
        this.store = store;
    }

    public void start() {

        logger.info("Starting " + getClass().getName());

        final String applicationId = config.getProperty("application.id");
        final String registryUrl = config.getProperty("schema.registry.url");

        logger.info("Registering applicationId: " + applicationId);

        final SchemaRegistryClient schemaRegistry = new CachedSchemaRegistryClient(registryUrl, 100);

        final Serde<CheckoutEvent> eventSerde = new EventSerdes(applicationId, schemaRegistry, registryUrl).getSerdes();
        final Serde<CheckoutSnapshot> snapshotSerde = new SnapshotSerdes(applicationId, schemaRegistry, registryUrl).getSerdes();

        final KStreamBuilder builder = new KStreamBuilder();
        // builder.newName(CheckoutProjector.class.getSimpleName() + "_" + INDEX);

        final KStream<String, CheckoutEvent> events = builder.stream(new Serdes.StringSerde(),
                eventSerde, StaticConfigs.EVENT_TOPIC);

        final KStream<String, CheckoutSnapshot> snapshots = builder.stream(new Serdes.StringSerde(),
                snapshotSerde, StaticConfigs.SNAPSHOT_TOPIC);

        final KStream<String, CheckoutSnapshot> results = events.join(snapshots, (event, snapshot) -> {
                    CheckoutSnapshot result = null;
                    //System.out.println(event.getManifest() + " vs " + ((Checkout) snapshot.getPayload()).getStatus());
                    //System.out.println(event.getId() + " vs " + snapshot.getPayload().getEventId());
                    if (event.getPayload().getId().equals(snapshot.getPayload().getEventId())) {
                        result = snapshot;
                    }
                    return result;
                }, JoinWindows.of(TimeUnit.SECONDS.toMillis(10)),
                new Serdes.StringSerde(), eventSerde, snapshotSerde).filter((k, v) -> v != null);


        results.foreach((k, v) -> {
            store.persist((Checkout) v.getPayload());
        });

        streams = new KafkaStreams(builder, config);

        streams.start();

        logger.info(getClass().getName() + " Started");
    }

    public void stop() {

        try {
            logger.info("Stopping " + getClass().getSimpleName());
            streams.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            logger.info(getClass().getSimpleName() + " Stopped");
        }
    }
}
