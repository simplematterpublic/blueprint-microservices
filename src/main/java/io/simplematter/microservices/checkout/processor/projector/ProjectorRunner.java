package io.simplematter.microservices.checkout.processor.projector;

import io.simplematter.microservices.common.stream.AbstractJob;
import io.simplematter.microservices.common.projection.Store;
import io.simplematter.microservices.checkout.protocol.internal.snapshot.Checkout;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.Properties;

public class ProjectorRunner extends AbstractJob {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final CheckoutProjector projector;

    public ProjectorRunner(final Properties properties, Store<String, Checkout> store) {

        super(properties);

        projector = new CheckoutProjector(getProperties(), store);
    }

    @Override
    public void start() {

        projector.start();
    }

    @Override
    public void stop() {

        projector.stop();
    }
}
