package io.simplematter.microservices.checkout.processor.projector;

import io.simplematter.microservices.checkout.protocol.internal.snapshot.Checkout;
import io.vertx.core.Handler;

import java.util.List;

public class ProjectorService {

    private final ProjectorStore store;

    public ProjectorService(final ProjectorStore store) {

        this.store = store;
    }

    public void byId(final String id, final Handler<Checkout> handler) {

        handler.handle(store.byId(id));
    }

    public void byIds(final List<String> ids, final Handler<List<Checkout>> handler) {

        handler.handle(store.byIds(ids));
    }

    public void all(final int limit, final Handler<List<Checkout>> handler) {

        handler.handle(store.all(limit));
    }
}
