package io.simplematter.microservices.checkout.processor.adapter;

import io.simplematter.microservices.checkout.StaticConfigs;
import io.simplematter.microservices.checkout.protocol.serdes.CommandSerdes;
import io.simplematter.microservices.checkout.protocol.serdes.SerdesFactory;
import io.simplematter.microservices.common.stream.AbstractJob;
import io.simplematter.microservices.common.protocol.Event;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.order.protocol.avro.OrderEventAvro;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.TopologyBuilder;

import java.util.HashMap;
import java.util.Properties;

public class AdapterRunner extends AbstractJob {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private KafkaStreams kafkaStreams;

    public AdapterRunner(final Properties properties) {

        super(properties);
    }

    @Override
    public void start() {

        logger.info("Starting " + getClass().getName());

        final String applicationId = getApplicationId();

        logger.info("Registering applicationId: " + applicationId);

        final SerdesFactory serdesFactory = new SerdesFactory(getApplicationId(), getRegistryUrl(), getSchemaRegistry());

        final AvroDeserializer<Event> orderDeserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(),
                new io.simplematter.microservices.order.protocol.converter.EventConverter(),
                OrderEventAvro.SCHEMA$);

        orderDeserializer.configure(new HashMap() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        final CommandSerdes commandSerdes = serdesFactory.commandSerdes();

        final TopologyBuilder topologyBuilder = new TopologyBuilder();

        topologyBuilder
                .addSource("ORDER_EVENT_SOURCE", new StringDeserializer(), orderDeserializer, "order_events_" + StaticConfigs.INDEX)
                .addProcessor("ORDER_ADAPTER_PROCESSOR", OrderAdapter::new, "ORDER_EVENT_SOURCE")
                .addSink("ORDER_COMMAND_SINK", StaticConfigs.COMMAND_TOPIC, new StringSerializer(),
                        commandSerdes.getSerializer(), "ORDER_ADAPTER_PROCESSOR");

        final StreamsConfig streamsConfig = new StreamsConfig(getProperties());

        kafkaStreams = new KafkaStreams(topologyBuilder, streamsConfig);

        kafkaStreams.start();

        logger.info(getClass().getName() + " Started");
    }

    @Override
    public void stop() {

        kafkaStreams.close();
    }
}
