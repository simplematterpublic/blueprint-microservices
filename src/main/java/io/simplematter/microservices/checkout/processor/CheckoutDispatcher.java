package io.simplematter.microservices.checkout.processor;

import io.simplematter.microservices.checkout.protocol.internal.CheckoutEvent;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutSnapshot;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutState;
import io.simplematter.microservices.checkout.protocol.internal.snapshot.Checkout;
import io.simplematter.microservices.common.kafka.streams.processor.StoreProcessor;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorSupplier;

public class CheckoutDispatcher implements ProcessorSupplier<String, CheckoutEvent> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String storeName;
    private final String eventDestination;
    private final String snapshotDestination;

    public CheckoutDispatcher(final String storeName, final String eventDestination, final String snapshotDestination) {

        this.storeName = storeName;
        this.eventDestination = eventDestination;
        this.snapshotDestination = snapshotDestination;
    }

    public Processor<String, CheckoutEvent> get() {

        return new StoreProcessor<String, CheckoutEvent, CheckoutState>() {

            @Override
            public String getStoreName() {

                return storeName;
            }

            public String getEventDestination() {

                return eventDestination;
            }

            public String getSnapshotDestination() {

                return snapshotDestination;
            }

            @Override
            public void process(final String key, final CheckoutEvent eventEnvelope) {

                logger.info("DispatcherProcessor received message: " + eventEnvelope.getPayload().getClass().getName());
                //System.out.println(getClass().getSimpleName() + " <- (" + key + ", " + checkoutEvent + ")");
                //System.out.println(getClass().getSimpleName() + " -> (" + key + ", " + checkoutEvent + "->SNAPSHOT)");
                final CheckoutSnapshot snapshotEnvelope = new CheckoutSnapshot(eventEnvelope.getId(), eventEnvelope.getCorrelationId());
                snapshotEnvelope.setPayload(Checkout.create(getState(key), eventEnvelope.getClass().getName()));
                context().forward(eventEnvelope.getId(), snapshotEnvelope, getSnapshotDestination());
                ///System.out.println("Forward snapshot to " + getSnapshotDestination());
                context().forward(eventEnvelope.getId(), eventEnvelope, getEventDestination());
                //System.out.println("Forward event to " + getEventDestination());
                context().commit();
            }
        };
    }

}
