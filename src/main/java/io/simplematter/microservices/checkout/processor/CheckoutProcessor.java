package io.simplematter.microservices.checkout.processor;

import io.simplematter.microservices.checkout.protocol.internal.CheckoutCommand;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutEvent;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutState;
import io.simplematter.microservices.checkout.protocol.internal.command.AcknowledgeCheckout;
import io.simplematter.microservices.checkout.protocol.internal.command.ConfirmCheckout;
import io.simplematter.microservices.checkout.protocol.internal.command.RejectCheckout;
import io.simplematter.microservices.checkout.protocol.internal.command.RequestCheckout;
import io.simplematter.microservices.checkout.protocol.internal.event.*;
import io.simplematter.microservices.checkout.protocol.internal.state.Checkout;
import io.simplematter.microservices.common.protocol.Command;
import io.simplematter.microservices.common.kafka.streams.processor.StoreProcessor;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorSupplier;

import java.util.UUID;

public class CheckoutProcessor implements ProcessorSupplier<String, CheckoutCommand> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String storeName;
    private final String eventDestination;
    private final String errorDestination;

    public CheckoutProcessor(final String storeName, final String eventDestination, final String errorDestination) {

        this.storeName = storeName;
        this.eventDestination = eventDestination;
        this.errorDestination = errorDestination;
    }

    public Processor<String, CheckoutCommand> get() {

        return new StoreProcessor<String, CheckoutCommand, CheckoutState>() {

            protected String getStoreName() {
                return storeName;
            }

            public String getEventDestination() {

                return eventDestination;
            }

            public String getErrorDestination() {

                return errorDestination;
            }

            @Override
            public void process(final String key, final CheckoutCommand value) {

                try {
                    logger.info("Processing command: " + value.getClass().getName());

                    //if (!value.isExpired()) {
                        doProcess(key, value);
                    //} else {

                    //}
                } catch (Exception e) {
                    logger.error(e);
                } finally {
                    logger.info("Committing on command: " + value.getClass().getName());
                    context().commit();
                }
            }

            /*
            public void onException(final String key, final CheckoutCommand commandEnvelope, final Throwable exception) {

                final CheckoutEvent eventEnvelope = new CheckoutEvent(commandEnvelope.getId(), commandEnvelope.getCorrelationId());
                final Checkout state = getState(eventEnvelope.getPayload().getId());
                if (state != null) {
                    final CheckoutState stateEnvelope = new CheckoutState(eventEnvelope.getId(), eventEnvelope.getCorrelationId());
                    state.setStatus(Checkout.REJECTED);
                    stateEnvelope.setPayload(state);
                    getStore().put(state.getId(), stateEnvelope);
                }
                final BusinessRejected businessRejected = new BusinessRejected();
                businessRejected.setId(commandEnvelope.getId());
                businessRejected.setOrigId(commandEnvelope.getId());
                businessRejected.setReason(exception.getMessage());
                businessRejected.setTimestamp(System.currentTimeMillis());
                context().forward(key, commandEnvelope, getErrorDestination());
            } */

            public void doProcess(final String key, final CheckoutCommand commandEnvelope) {

                final CheckoutState stateEnvelope = new CheckoutState(commandEnvelope.getId(), commandEnvelope.getCorrelationId());
                final CheckoutEvent eventEnvelope = new CheckoutEvent(commandEnvelope.getId(), commandEnvelope.getCorrelationId());
                final Command command = commandEnvelope.getPayload();
                if (command instanceof RequestCheckout) {
                    final Checkout currentState = getState(((RequestCheckout) command).getId());
                    if (currentState == null || currentState.getStatus().equals(Checkout.REQUESTED)) {
                        final CheckoutRequested event = request((RequestCheckout) command);
                        eventEnvelope.setPayload(event);
                        logger.info("Processing event " + event.getClass().getName());
                        final Checkout state = Checkout.create(event, context().offset());
                        stateEnvelope.setPayload(state);
                        getStore().put(state.getId(), stateEnvelope);
                        context().forward(state.getId(), eventEnvelope, getEventDestination());
                    } else {
                        throw new RuntimeException("Invalid state");
                    }
                } else if (command instanceof AcknowledgeCheckout) {
                    final Checkout currentState = getState(((AcknowledgeCheckout) command).getId());
                    if (currentState != null && (Checkout.REQUESTED.equals(currentState.getStatus()) ||
                            Checkout.ACKNOWLEDGED.equals(currentState.getStatus()))) {
                        final CheckoutAcknowledged event = acknowledge((AcknowledgeCheckout) command);
                        eventEnvelope.setPayload(event);
                        final Checkout state = currentState.update(event, context().offset());
                        stateEnvelope.setPayload(state);
                        getStore().put(state.getId(), stateEnvelope);
                        context().forward(state.getId(), eventEnvelope, getEventDestination());
                    } else {
                        throw new RuntimeException("Invalid state or state not found for id: " + command.getId());
                    }
                } else if (command instanceof ConfirmCheckout) {
                    final Checkout currentState = getState(((ConfirmCheckout) command).getId());
                    if (currentState != null && (currentState.getStatus().equals(Checkout.ACKNOWLEDGED) ||
                            currentState.getStatus().equals(Checkout.CONFIRMED))) {
                        final CheckoutConfirmed event = confirm((ConfirmCheckout) command);
                        eventEnvelope.setPayload(event);
                        final Checkout state = currentState.update(event, context().offset());
                        stateEnvelope.setPayload(state);
                        getStore().put(state.getId(), stateEnvelope);
                        context().forward(state.getId(), eventEnvelope, getEventDestination());
                    } else {
                        throw new RuntimeException("Invalid state");
                    }
                } else if (command instanceof RejectCheckout) {
                    final Checkout currentState = getState(((RejectCheckout) command).getId());
                    if (currentState != null) {
                        final CheckoutRejected event = reject((RejectCheckout) command);
                        eventEnvelope.setPayload(event);
                        final Checkout state = currentState.update(event, context().offset());
                        stateEnvelope.setPayload(state);
                        getStore().put(state.getId(), stateEnvelope);
                        context().forward(state.getId(), eventEnvelope, getEventDestination());
                    } else {
                        throw new RuntimeException("Invalid state");
                    }
                } else {
                    throw new RuntimeException("Received unknown message: " + command.getClass().getName());
                }
            }

            /**
             *
             * @param command The Checkout Command
             * @return The Checkout Event
             */
            private CheckoutRequested request(final RequestCheckout command) {

                final CheckoutRequested event = new CheckoutRequested();
                event.setId(UUID.randomUUID().toString());
                event.setSku(command.getSku());
                event.setQuantity(command.getQuantity());
                event.setPrice(command.getPrice());
                event.setPaymentDetail(command.getPaymentDetail());
                event.setCustomerId(command.getCustomerId());
                event.setTimestamp(System.currentTimeMillis());
                return event;
            }

            /**
             *
             * @param command The Checkout Command
             * @return The Checkout Event
             */
            private CheckoutAcknowledged acknowledge(final AcknowledgeCheckout command) {

                final CheckoutAcknowledged event = new CheckoutAcknowledged();
                event.setId(UUID.randomUUID().toString());
                event.setOrderId(command.getOrderId());
                event.setTimestamp(System.currentTimeMillis());
                return event;
            }

            /**
             *
             * @param command The Checkout Command
             * @return The Checkout Event
             */
            private CheckoutConfirmed confirm(final ConfirmCheckout command) {

                final CheckoutConfirmed event = new CheckoutConfirmed();
                event.setId(UUID.randomUUID().toString());
                event.setOrderId(command.getOrderId());
                event.setPaymentId(command.getPaymentId());
                event.setTimestamp(System.currentTimeMillis());
                return event;
            }

            /**
             *
             * @param command The Checkout Command
             * @return The Checkout Event
             */
            private CheckoutRejected reject(final RejectCheckout command) {

                final CheckoutRejected event = new CheckoutRejected();
                event.setId(UUID.randomUUID().toString());
                event.setReason(command.getReason());
                event.setTimestamp(System.currentTimeMillis());
                return event;
            }
        };
    }
}
