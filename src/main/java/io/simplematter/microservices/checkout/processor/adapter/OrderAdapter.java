package io.simplematter.microservices.checkout.processor.adapter;

import io.simplematter.microservices.checkout.protocol.internal.CheckoutCommand;
import io.simplematter.microservices.checkout.protocol.internal.command.AcknowledgeCheckout;
import io.simplematter.microservices.checkout.protocol.internal.command.ConfirmCheckout;
import io.simplematter.microservices.checkout.protocol.internal.command.RejectCheckout;
import io.simplematter.microservices.common.protocol.Command;
import io.simplematter.microservices.common.protocol.Event;
import io.simplematter.microservices.order.protocol.internal.OrderEvent;
import io.simplematter.microservices.order.protocol.internal.event.OrderConfirmed;
import io.simplematter.microservices.order.protocol.internal.event.OrderRejected;
import io.simplematter.microservices.order.protocol.internal.event.OrderValidated;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.streams.processor.AbstractProcessor;

public class OrderAdapter extends AbstractProcessor<String, OrderEvent> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void process(final String key, final OrderEvent orderEnvelope) {

        try {
            doProcess(key, orderEnvelope);
        } catch (Exception e) {
            logger.error(e);
        } finally {
            context().commit();
        }
    }

    private void doProcess(final String key, final OrderEvent orderEnvelope) {

        final CheckoutCommand checkoutCommand = new CheckoutCommand(orderEnvelope.getId(), orderEnvelope.getCorrelationId());
        final Event event = orderEnvelope.getPayload();
        logger.info("AdapterProcessor received message: " + orderEnvelope.getPayload().getClass().getName());
        if (event instanceof OrderValidated) {
            checkoutCommand.setPayload(convert((OrderValidated) event));
            context().forward(key, checkoutCommand);
        } else if (event instanceof OrderConfirmed) {
            checkoutCommand.setPayload(convert((OrderConfirmed) event));
            context().forward(key, checkoutCommand);
        } else if (event instanceof OrderRejected) {
            checkoutCommand.setPayload(convert((OrderRejected) event));
            context().forward(key, checkoutCommand);
        } else {
            if (logger.isDebugEnabled()) {
                logger.warn("AdapterProcessor skipping unhandled message: " + orderEnvelope.getPayload().getClass().getName());
            }
        }
    }

    private Command convert(final OrderValidated event) {

        final AcknowledgeCheckout command = new AcknowledgeCheckout();
        command.setId(event.getCheckoutId());
        command.setOrderId(event.getId());
        command.setTimestamp(System.currentTimeMillis());
        return command;
    }

    private Command convert(final OrderConfirmed event) {

        final ConfirmCheckout command = new ConfirmCheckout();
        command.setId(event.getCheckoutId());
        command.setOrderId(event.getId());
        command.setPaymentId(event.getPaymentId());
        command.setTimestamp(System.currentTimeMillis());
        return command;
    }

    private Command convert(final OrderRejected event) {

        final RejectCheckout command = new RejectCheckout();
        command.setId(event.getCheckoutId());
        command.setReason(event.getReason());
        command.setOrderId(event.getId());
        command.setTimestamp(event.getTimestamp());
        return command;
    }
}
