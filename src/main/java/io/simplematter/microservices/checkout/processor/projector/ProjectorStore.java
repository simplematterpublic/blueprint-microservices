package io.simplematter.microservices.checkout.processor.projector;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.model.MapObject;
import com.rethinkdb.net.Connection;
import io.simplematter.microservices.checkout.protocol.internal.snapshot.Checkout;
import io.simplematter.microservices.common.projection.AbstractStore;
import io.simplematter.microservices.common.util.TypeUtil;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.HashMap;

public class ProjectorStore extends AbstractStore<String, Checkout> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public static final RethinkDB r = RethinkDB.r;

    public ProjectorStore(final Connection connection, final String table) {

        super(connection, table);
    }

    @Override
    protected Checkout convert(final HashMap source) {

        final Checkout sink = new Checkout();
        sink.setId((String) source.get("id"));
        sink.setSku((String) source.get("sku"));
        sink.setQuantity(TypeUtil.toInteger(source.get("quantity")));
        sink.setPrice(TypeUtil.toDouble(source.get("price")));
        sink.setCustomerId((String) source.get("customerId"));
        sink.setOrderId((String) source.get("orderId"));
        sink.setPaymentId((String) source.get("paymentId"));
        sink.setStatus((String) source.get("status"));
        sink.setEventId((String) source.get("eventId"));
        sink.setEventName((String) source.get("eventName"));
        sink.setTimestamp((Long) source.get("timestamp"));
        return sink;
    }

    @Override
    protected MapObject convert(final Checkout source) {

        return r.hashMap("id", source.getId())
                .with("sku", source.getSku())
                .with("quantity", source.getQuantity())
                .with("price", source.getPrice())
                .with("customerId", source.getCustomerId())
                .with("orderId", source.getOrderId())
                .with("paymentId", source.getPaymentId())
                .with("status", source.getStatus())
                .with("eventId", source.getEventId())
                .with("eventName", source.getEventName())
                .with("timestamp", source.getTimestamp());
    }
}
