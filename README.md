SimpleMatter Microservice Demo
====================================
This is an example showing a simple Checkout e-commerce scenario consisting of three Microservices based on [Vert.x][http://vertx.io/],
[Apache Kafka][https://kafka.apache.org/], [RethinkDB][https://www.rethinkdb.com/] and [Apache Avro][https://avro.apache.org/].

It also demonstrates how to use Snapshot Propagation in order to integrate different Microservices.

:warning: **Please note that in a real scenario there would have been three different project repositories, Checkout, Order and Payment.** However for
simplicity and demonstration purposes short cuts have been taken and everything has been merged in one single project.

Furthermore, for the same reason, no Docker, Service Orchestration, Service Discovery, or Clustering has been implemented.

Prerequisites
================
In order to run the services the following installations are required:
* Apache Kafka `0.10.1`
* Kafka Schema Registry [https://github.com/confluentinc/schema-registry]
* RethinkDB `2.3.6`
* Apache Maven 3.5.0

Instructions
================

Run Kafka Zookeeper
**bin/zookeeper-server-start.sh config/zookeeper.properties**

Run Kafka Server
**bin/kafka-server-start.sh config/server.properties**

Create Kafka Topics
- **bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic checkout_commands_1**
- **bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic checkout_events_1**
- **bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic checkout_snapshots_1**

- **bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic order_commands_1**
- **bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic order_events_1**
- **bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic order_snapshots_1**

- **bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic payment_commands_1**
- **bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic payment_events_1**
- **bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic payment_snapshots_1**

Run Kafka Schema Registry
**bin/schema-registry-start ./etc/schema-registry/schema-registry.properties**

Run RethinkDB Server
**bin/schema-registry-start ./etc/schema-registry/schema-registry.properties**

Run Maven
**/bin/mvn compile package**

Run Checkout Service
**java -cp target/blueprint-microservices-1.0-SNAPSHOT-jar-with-dependencies.jar io.simplematter.microservices.payment.PaymentService**

Run Order Service
**java -cp target/blueprint-microservices-1.0-SNAPSHOT-jar-with-dependencies.jar io.simplematter.microservices.order.OrderService**

Run Payment Service
**java -cp target/blueprint-microservices-1.0-SNAPSHOT-jar-with-dependencies.jar io.simplematter.microservices.payment.PaymentService**

Run Checkout Simulator
**java -cp target/blueprint-microservices-1.0-SNAPSHOT-jar-with-dependencies.jar io.simplematter.microservices.simulation.CheckoutSimulator interval** E.g. 1000 = 1 second 

Check REST APIs
- **http://localhost:9001/api/checkouts**
- **http://localhost:9002/api/orders**
- **http://localhost:9003/api/payments**


